/*! \file
@author Author
- Name: Sourav Garg
- Affiliation: TCS Innovation Lab, New Delhi
- Email: garg.sourav@tcs.com, sourav.sahaji@gmail.com
- Date: Jun 24, 2015
*/

#include<stdio.h>
#include<iostream>
#include<fstream>

#include<opencv2/core/core.hpp>
#include<opencv2/highgui/highgui.hpp>
#include<opencv2/imgproc/imgproc.hpp>
#include<opencv2/video/tracking.hpp>
#include<opencv2/objdetect/objdetect.hpp>
#include<opencv2/calib3d/calib3d.hpp>
#include<opencv2/nonfree/nonfree.hpp>

// LibDAI Headers
#include<dai/bp.h>
#include<dai/jtree.h>
#include<dai/factorgraph.h>

// TinyXML Headers
#include<tinyxml.h>
#include<tinystr.h>

#include<pedestrian_detection_multitracking.h>

#define TIMER_FILE              1           // Switch to decide writing the timer files
#define VIDEO_OUT               1           // Switch to decide writing video output
#define SHOW_IMAGE              1           // Switch to display image.
int WAITKEY = 1;                            // For Pause/Play of display - Press P/p to play/pause the display

#define SHOW_DETECTIONS         0           // Shows an image with detected rectangles marked

#define HIST_BIN_SIZE           16          // Size of bin for histogram computation
#define RUN_AVG                 0           // file i/o: Calculate running average time?
#define IND_TIME                0           // file i/o: Only individual time calc?
#define DEBUG_STATES            0           // Switch to print the data for tracker states

#define exitThre                15          // Threshold to decide how long to predict an agent without confirmation

// Mean Shift Parameters
#define EPSILON                 0.00001
#define PI                      3.14159265
#define RANSAC_THREHOLD         10
#define orientHist              0
#define fourDimHist             0
#define rgbHist                 1

int globalID =1;                            // Tracked object's ID
float KF_dt = 1;


#define LINENAME    CV_AA

#define KF_PAIRS                1           // Defining KF pair model
#define KF_PAIRS_PRED           0           // Prediction based on KF_PAIRS


using namespace cv;
using namespace std;

void calcHistogram(Mat srcImg, Mat& hist, int bins );
void scaleRect(Rect& ioRoi, float percentScale);
float findOverlap(Rect r1, Rect r2);
void getHist(Mat frameData, Rect reg, Mat& hist, int bins = HIST_BIN_SIZE);

class MultiTracker
{
private:

protected:

public:

    Rect MS_Rect;       //Mean Shift predicted window
    Rect trackWindow;  //position of detected agent

    //    KalmanFilter_ KF;
    KalmanFilter KF_CV;//Kalman Filter openCV

    Rect KF_RectPredicted;  //predicted region through kalman filter
    Rect prevWindow;

    int bins; //no. of bins of the histogram
    int orientbins; //no. of orientation bins
    Mat hist,rgb_hist,orient_hist,fourDim_hist; //histogram of the agent
    Mat grad; //gradient of tracked window of the agent
    Mat orient; //orients of tracked window of the image

    int id; //agent ID
    bool status;        // active=true or passive=false
    int  exitTimeSpan;  // remove the agent passive for duration greater than this
    int activeTimeSpan;
    unsigned char red,green,blue;   //Color of the agent
    vector<Point2i> traj, activeTraj; //Vector storing trajectory for the agent
    float avgVelocityDirection;
    float scale;

    // Mean Shift Parameters
    float Epsilon;
    float PrevBhatCoeff;
    float CurrBhatCoeff;
    float ItrNumBound;
    float SubItrNumBound;
    float ItrCond;
    bool UpdateScaleAfterIterations;
    float XScaleChange;
    float YScaleChange;
    bool ToTrack;
    bool MeanShiftTrackerDataReady;
    Rect PrevRegion;
    Rect CurrRegion;
    Mat TargetHist;
    Mat PrevPosHist;
    Mat CurrPosHist;
    int ItrNum;

    // Parameters for groups resolution (Not significant currently)
    Rect initialAssignment;     // This is the assigned roi for an agent, changes can be made and then finally passed to updateAgent
    Mat agentImgSolo;           // A snapshot image of an agent when it is not in a group
    bool inGroup;               // Flag that tells if the agent is in a group or not
    bool dead;                  // If an agent has died (i.e. been passive for certain number of frames (exitThresh) )

    Mat agentImgUpdated;            // The image of agent after a tracker of that agent is updated

    // Extra parameters for writing result data in XML
    vector<int> frameIds;       // vector of frame number in which this id was detected
    vector<Rect> frameROIs;     // vector of all rectangles detected for this id
    vector<int> frameObservationFlag;   // vector of observation flag (1 for detection 0 for prediction)

    // Parameters for agent states
    int agentState;     // agent state = 1 is active, 2 is InterObjectOcclusion, 3 is BG occlusion, 4 is Lack of confirmation (refine 3,4)
    int occludingAgentID;   // ID of the agent who has occluded the current agent
    map< int,pair<float,float> > relation;  // mapping with the other agents (ids) and the corresponding dij and occ_prob
    map< int,Rect > relationROIs;
    int coPedId;            // ID of the agent who is probably walking together with this agent
    Rect initialPrediction; // Required for setting a prediction window using SURF

    MultiTracker();
    void initAgent(Mat img, Rect agnPos, int frameId, vector<Rect> activeAgentsWin, bool appearanceModel);   //Initializes newly detected agents.
    void updateAgent(Mat img, Rect agnPos, int frameId, vector<Rect> activeAgentsWin, bool appearanceModel); //Updates active agents if they match detected windows.
    void computeColorDistributionRGB(Mat img, Rect agnPos,Mat &hist); //Find RGB Histogram of provided rectangle in image.
    void markAgents(Mat img);   //For plotting agent position.
    void gradImage(Mat img, Rect agnWin, Mat& grad, Mat& orient); //Calculates gradient magnitudes of the pixels in the agent window.
    void calcOrientHist(Mat img,Rect agnPos);

    void meanShiftTrack(Mat& image, Rect& selection, int& trackObject);

    void localizeAppearanceByMeanShift( Mat img, float bhatCoefThreshold, float* matchVal );
    void meanShiftTrackingRGB( Mat frameData , float bhatCoeffThreshold );
    void setMeanShiftTrackerTarget( Mat targetHistRGB , Rect initReg );
    void iterateRGB( Mat frameData );
    void updateScaleRGB( Mat frameData );
    void copyHistogram( Mat srcHist ,Mat destHist );
    void getNewRegionRGB( Mat frameData , Rect&  newRegion );

    //    ~MultiTracker();
};


unsigned char computeBhattacharyaCoefficient( Mat hist1, Mat hist2, float* bhatCoefVal );
void calcOverlap(Point tl1, Point tl2, Size sz1, Size sz2, float *overlap);
void checkBoundary(Mat frameData, Rect& tempRect);

MultiTracker::MultiTracker()
{
    bins = HIST_BIN_SIZE;
    orientbins = HIST_BIN_SIZE;
    hist = Mat(1, pow(bins,3), CV_32FC1, Scalar(0));

    if(rgbHist)
    {
        TargetHist = Mat(1, pow(bins,3) ,CV_32FC1,Scalar(0));
        PrevPosHist = Mat(1, pow(bins,3),CV_32FC1,Scalar(0));
        CurrPosHist = Mat(1, pow(bins,3),CV_32FC1,Scalar(0));
    }

    if(orientHist)
    {
        TargetHist = Mat(1, pow(orientbins,3) ,CV_32FC1,Scalar(0));
        PrevPosHist = Mat(1, pow(orientbins,3),CV_32FC1,Scalar(0));
        CurrPosHist = Mat(1, pow(orientbins,3),CV_32FC1,Scalar(0));
    }

    if(fourDimHist)
    {
        TargetHist = Mat(1, pow(orientbins,3) ,CV_32FC1,Scalar(0));
        PrevPosHist = Mat(1, pow(orientbins,3),CV_32FC1,Scalar(0));
        CurrPosHist = Mat(1, pow(orientbins,3),CV_32FC1,Scalar(0));
    }

    orient_hist = Mat(1, pow(orientbins,3), CV_32FC1, Scalar(0));
    rgb_hist = Mat(1, pow(bins,3), CV_32FC1, Scalar(0));
    fourDim_hist = Mat(1, pow(orientbins,3), CV_32FC1, Scalar(0));
}


/*!
  @brief Initializes Agents detected for first time.
  @param[in] img Input image for agent properties.
  @param[in] agnPos Position of agent in the given image.
*/
void MultiTracker::initAgent(Mat img, Rect agnPos, int frameId, vector<Rect> activeAgentsWin, bool trainingGMM = false)
{

    prevWindow = agnPos;
    trackWindow = agnPos;
    MS_Rect = agnPos;
    Mat img2 = img.clone();

    computeColorDistributionRGB(img2 , agnPos, hist);

    gradImage(img2,trackWindow, grad, orient);
    calcOrientHist(img2, agnPos);

    exitTimeSpan = 0;
    status = true;
    activeTimeSpan = 1;

    //Generating random values for random color generation of the agents
    timespec t1;
    clock_gettime(0,&t1);
    RNG rng(t1.tv_nsec);

    red = rng.uniform(0,255);
    green = rng.uniform(0,255);
    blue = rng.uniform(0,255);

    // Setting the Mean-Shist Tracker Parameters : Initialising Variables
    Epsilon = 1.0;
    PrevBhatCoeff = 0.0;
    ItrNumBound = 5;
    SubItrNumBound = 5;
    ItrCond = 255;
    UpdateScaleAfterIterations = true;
    XScaleChange = 0.0;
    YScaleChange = 0.0;
    ToTrack = true;
    MeanShiftTrackerDataReady = false;

    activeTraj.push_back(Point2i(agnPos.x+agnPos.width/2,agnPos.y+agnPos.height/2));
    avgVelocityDirection = 0;

    KF_dt = 0.5;

    KF_CV.init(4,2,0);
    KF_CV.transitionMatrix = (Mat_<float>(4, 4) << 1,0,KF_dt,0,0,1,0,KF_dt,0,0,1,0,0,0,0,1); // A
    KF_CV.statePre.at<float>(0) = agnPos.x+agnPos.width/2;
    KF_CV.statePre.at<float>(1) = agnPos.y+agnPos.height/2;
    Mat measure = Mat(0.5*(trackWindow.tl()+trackWindow.br()));

    setIdentity(KF_CV.measurementMatrix); // H
    setIdentity(KF_CV.processNoiseCov, Scalar::all(0.01)); // Q
    setIdentity(KF_CV.measurementNoiseCov, Scalar::all(0.01)); // R
    setIdentity(KF_CV.errorCovPost, Scalar::all(0.01)); // P

    //    randn(KF_CV.statePost, Scalar::all(0), Scalar::all(0.1));
    //    KF_CV.statePost = KF_CV.statePre.clone();


    measure.convertTo(measure,CV_32FC1);
    KF_CV.correct(measure);

    KF_CV.predict();

    KF_RectPredicted.width = agnPos.width;
    KF_RectPredicted.height = agnPos.height;

    KF_RectPredicted.x = KF_CV.statePost.at<float>(0)-KF_RectPredicted.width/2;
    KF_RectPredicted.y = KF_CV.statePost.at<float>(1)-KF_RectPredicted.height/2;

    checkBoundary(img,KF_RectPredicted);

    initialAssignment = Rect(0,0,0,0);
    agentImgSolo = img(agnPos).clone();
    inGroup = false;
    dead = false;

    agentImgUpdated = img(agnPos).clone();

    agentState = 1;
    occludingAgentID = -1;

    coPedId = -1;
    initialPrediction = Rect(0,0,0,0);
}


/*!
  @brief Updates agent with HogWin associtaed to it.
  @param[in] img Input image for agent properties.
  @param[in] agnPos Position of agent in the given image.
*/
void MultiTracker::updateAgent(Mat img, Rect agnPos, int frameId, vector<Rect> activeAgentsWin, bool trainingGMM = false)
{
    prevWindow = trackWindow;
    scale = (agnPos.area())/(64.0*128.0);

    KF_RectPredicted.width = trackWindow.width;
    KF_RectPredicted.height = trackWindow.height;

    Mat imgTemp = img.clone();

    trackWindow = agnPos;

    Mat oldHist = hist.clone();
    computeColorDistributionRGB(imgTemp , agnPos, hist);
    Mat avgHist;
    add(oldHist,hist,avgHist);
    avgHist *= 1/sum(avgHist)[0];

    hist = avgHist.clone();

    gradImage(imgTemp,agnPos, grad, orient);
    calcOrientHist(imgTemp, agnPos);

    exitTimeSpan = 0;
    status = true;
    activeTimeSpan++;
    dead = false;

    setIdentity(KF_CV.measurementMatrix,Scalar::all(1));
    Mat measure = Mat(0.5*(trackWindow.tl()+trackWindow.br()));
    measure.convertTo(measure,CV_32FC1);
    KF_CV.correct(measure);

    KF_CV.predict();

    KF_RectPredicted.x = KF_CV.statePost.at<float>(0) - KF_RectPredicted.width/2;
    KF_RectPredicted.y = KF_CV.statePost.at<float>(1) - KF_RectPredicted.height/2;
    checkBoundary(img, KF_RectPredicted);

    // Setting the Mean-Shist Tracker Parameters : Initialising Variables
    Epsilon = 1.0;
    PrevBhatCoeff = 0.0;
    ItrNumBound = 5;
    SubItrNumBound = 5;
    ItrCond = 255;
    UpdateScaleAfterIterations = true;
    XScaleChange = 0.0;
    YScaleChange = 0.0;
    ToTrack = true;
    MeanShiftTrackerDataReady = false;


    activeTraj.push_back(Point2i(agnPos.x+agnPos.width/2,agnPos.y+agnPos.height/2));
    Point2f currPosn = activeTraj[activeTraj.size()-1];
    Point2f prevPosn = activeTraj[activeTraj.size()-2];
    float instantVelDir = atan2(currPosn.y-prevPosn.y,currPosn.x-prevPosn.x);
    avgVelocityDirection = (instantVelDir + avgVelocityDirection*(activeTraj.size()-2))/(activeTraj.size()-1);

    if(!frameIds.empty())
    {
        if(frameIds[frameIds.size()-1] != frameId)
        {
            frameIds.push_back(frameId);
            frameROIs.push_back(agnPos);
            frameObservationFlag.push_back(1);
        }
    }
    else
    {
        frameIds.push_back(frameId);
        frameROIs.push_back(agnPos);
        frameObservationFlag.push_back(1);
    }

    if(!inGroup)
        agentImgSolo = img(agnPos).clone();
    agentImgUpdated = img(agnPos).clone();

    initialAssignment = Rect(0,0,0,0);

    //    if(occludingAgentID != -1)
    //    {
    //        relation[occludingAgentID].second = 0.5;
    //        relation[id].second = 0.5;
    //    }

    agentState = 1;
    occludingAgentID = -1;

    initialPrediction = Rect(0,0,0,0);
}


/*!
  @brief Finds HOG windows in the given image.
  @param[in] img Input image for detection.
  @param[in,out] found_filtered Output detected windows.
  @return Number of detections.
*/
int HogDetector(Mat img, vector<Rect>& found_filtered, float hitRate = 0.0)
{
    found_filtered.clear();
    int rect_num=0;

    HOGDescriptor hog;
    //    hog.nlevels = 200;
    hog.setSVMDetector(HOGDescriptor::getDefaultPeopleDetector());

    vector<Rect> found;
    vector<Rect> tempRects;
    // run the detector with default parameters. to get a higher hit-rate
    // (and more false alarms, respectively), decrease the hitThreshold and
    // groupThreshold (set groupThreshold to 0 to turn off the grouping completely).
    hog.detectMultiScale(img, found, hitRate, Size(8,8), Size(0,0), 1.05);
    size_t i, j;
    for( i = 0; i < found.size(); i++ )
    {
        Rect r = found[i];
        for( j = 0; j < found.size(); j++ )
            if( j != i && (r & found[j]) == r)
                break;
        if( j == found.size() )
            tempRects.push_back(r);
    }
    for( i = 0; i < tempRects.size(); i++ )
    {
        Rect r = tempRects[i];
        // the HOG detector returns slightly larger rectangles than the real objects.
        // so we slightly shrink the rectangles to get a nicer output.
        r.x += cvRound(r.width*0.125);
        r.width = cvRound(r.width*0.7);
        r.y += cvRound(r.height*0.1);
        r.height = cvRound(r.height*0.75);
        //    rectangle(img, r.tl(), r.br(), cv::Scalar(0,255,0), 3);
        found_filtered.push_back(r);
    }
    rect_num=found_filtered.size();
    return( rect_num);
}



/*!
  @brief Finds colour distribution of given location in the image.
  @param[in] frameData Input image for histogram extraction.
  @param[in] reg rectangle to location in the image.
  @param[in] hist Histogram to be calculated for the specified rectangle in the image.
*/
void MultiTracker::computeColorDistributionRGB(Mat frameData, Rect reg, Mat& hist)
{
    reg.x += cvRound(reg.width*0.2);
    reg.width = cvRound(reg.width*0.6);
    reg.y += cvRound(reg.height*0.1);
    reg.height = cvRound(reg.height*0.8);

    for(int i=0; i<hist.cols; i++)
        hist.at<float>(0,i)=0.0;

    // Computing the Right Bottom of the Region
    int rightBotX = ( reg.x ) + ( reg.width );
    int rightBotY = ( reg.y ) + ( reg.height );

    // Computing Color Distribution from the Rectangular/Elliptical Region
    float sumW = 0.0;
    float invBinSize = ( (float) ( bins ) ) / 256.0;
    int binSqr = ( bins ) * ( bins );

    for( int y = ( reg.y ) ; y < rightBotY ; ++y )
    {
        for( int x = ( reg.x ) ; x < rightBotX ; ++x )
        {
            Vec3b color = frameData.at< Vec3b >(y,x);

            // Compute the Bin
            int redBin = (int) (invBinSize * ( (unsigned char) color[2]));
            int greenBin = (int) (invBinSize * ( (unsigned char) color[1]));
            int blueBin = (int) (invBinSize * ( (unsigned char) color[0]));

            // Compute the Histogram Index
            int histIndx = (redBin * binSqr ) + ( greenBin * bins ) + blueBin;

            // Update Histogram and Weight Sum
            hist.at<float>(0,histIndx ) = hist.at<float>(0,histIndx ) + 1;

            sumW = sumW + 1;
        }
    }

    // Normalizing the Histogram
    if( sumW > EPSILON )
    {
        float inv_sumW = 1.0 / sumW;
        for( int histIndx = 0 ; histIndx < hist.cols  ; ++histIndx )
        {
            hist.at<float>(0, histIndx) = inv_sumW * ( hist.at<float>(0,histIndx ) );
        }
    }
}



/*!
  @brief Finds orientation/HSO histogram.
  @param[in] frameData Input image for histogram calculation.
  @param[in] reg Portion where histogram to be calculated in the image.
*/
void MultiTracker::calcOrientHist(Mat frameData, Rect reg)
{
    if(fourDimHist)
    {
        for(int i=0; i<fourDim_hist.cols; i++)
            fourDim_hist.at<float>(0,i) = 0.0;
    }

    if(orientHist)
    {
        for(int i=0; i<orient_hist.cols; i++)
            orient_hist.at<float>(0,i) = 0.0;
    }

    int tempbins = HIST_BIN_SIZE;
    // Computing the Right Bottom of the Region
    int rightBotX = ( reg.x ) + ( reg.width );
    int rightBotY = ( reg.y ) + ( reg.height );

    // Computing Color Distribution from the Rectangular/Elliptical Region
    float sumW = 0.0;
    float invBinSize = ( (float) ( tempbins ) ) / 256.0;
    int binSqr = ( tempbins ) * ( tempbins );

    /************************
        for orientations-3D histogram
     ************************/

    Mat tempGradients, tempOrientations; //calculating gradients and orientations for histogram calculation for a window
    gradImage(frameData, reg, tempGradients, tempOrientations);

    for( int y = ( reg.y ) ; y < rightBotY ; ++y )
    {
        for( int x = ( reg.x ) ; x < rightBotX ; ++x )
        {
            Vec3f orientBin = tempOrientations.at<Vec3f>(y-reg.y, x-reg.x);

            int histIndx;

            if(fourDimHist)
            {
                Vec3b color = frameData.at<Vec3b>(y,x);

                int max;

                if(orientBin[0]>=orientBin[1] && orientBin[0]>=orientBin[2]) max = orientBin[0];
                if(orientBin[1]>=orientBin[0] && orientBin[1]>=orientBin[2]) max = orientBin[1];
                if(orientBin[2]>=orientBin[0] && orientBin[2]>=orientBin[1]) max = orientBin[2];

                // Compute the Bin
                int redBin = (int) (invBinSize * ( (float) color[2]));
                int greenBin = (int) (invBinSize * ( (float) color[1]));
                int blueBin = (int) (invBinSize * ( (float) color[0]));

                histIndx = (redBin * pow(tempbins,2) ) + ( greenBin * pow(tempbins,1) ) /*+ (blueBin*tempbins) */+ max;
            }

            if(orientHist)
                histIndx = (orientBin[2] * pow(orientbins,2) ) + ( orientBin[1] * pow(orientbins,1) ) + orientBin[0];


            // Update Histogram and Weight Sum

            if(orientHist)
                orient_hist.at<float>(0,histIndx ) = orient_hist.at<float>(0,histIndx ) + 1;

            if(fourDimHist)
                fourDim_hist.at<float>(0,histIndx ) = fourDim_hist.at<float>(0,histIndx ) + 1;

            sumW = sumW + 1;
        }
    }

    float inv_sumW = 1.0 / sumW;

    // Normalizing the Histogram
    if( sumW > EPSILON )
    {
        int cols;
        if(fourDimHist) cols = fourDim_hist.cols;
        if(orientHist) cols = orient_hist.cols;

        for( int histIndx = 0 ; histIndx < cols  ; ++histIndx )
        {
            if(fourDimHist)
                fourDim_hist.at<float>(0, histIndx) = inv_sumW * ( fourDim_hist.at<float>(0,histIndx ) );

            if(orientHist)
                orient_hist.at<float>(0, histIndx) = inv_sumW * ( orient_hist.at<float>(0,histIndx ) );
        }
    }
}



/*!
  @brief Calculates gradient of the agent provided throug agent window.
  @param[in] frameData image from which gradient & orientation to be calculated.
  @param[in] angWin Portion for which gradient & orienations to be calculated in the image.
  @param[in,out] grad Matrix into which gradient mag of the specified location are stored.
  @param[in,out] orient Matrix into which orientations of the specified location are stored.
*/
void MultiTracker::gradImage(Mat frameData, Rect agnWin, Mat& grad, Mat& orient)
{
    grad = Mat(agnWin.height, agnWin.width, CV_32FC3 ,Scalar(0,0,0));

    orient = Mat(agnWin.height, agnWin.width, CV_32FC3,Scalar(0,0,0));

    //    grad = frameData.rowRange(agnWin.y, agnWin.y+agnWin.height).colRange(agnWin.x, agnWin.x+agnWin.width).clone();
    //    GaussianBlur( grad, grad, Size(3,3), 0, 0 );
    //    Laplacian(grad, grad, CV_16S, 3, 1, 1);
    //    convertScaleAbs( grad, grad);

    Mat tempGrad = Mat(agnWin.height, agnWin.width, CV_8UC3,Scalar(0,0,0));
    tempGrad = frameData.rowRange(agnWin.y, agnWin.y+agnWin.height).colRange(agnWin.x, agnWin.x+agnWin.width).clone();

    GaussianBlur( tempGrad, tempGrad, Size(3,3), 0, 0 );

    Mat tempGrad1 = Mat(agnWin.height+2, agnWin.width+2, CV_8UC3,Scalar(0,0,0)); //window appended with zeros to calculate gradients
    tempGrad.copyTo(tempGrad1.rowRange(1, tempGrad1.rows-1).colRange(1, tempGrad1.cols-1));

    float binSize = 180.f/(orientbins);
    //    cout << binSize << "\t" << 180.0/orientbins << "\t" << 180.0/float(orientbins) << "\t" << 180/float(orientbins) << endl;

    for(int y=1; y<tempGrad1.rows-1; y++)
    {
        for(int x=1; x<tempGrad1.cols-1; x++)
        {
            Vec3f mag;
            Vec3f angle;
            Vec3b magdown = tempGrad1.at<Vec3b> (y+1,x);   Vec3b magup = tempGrad1.at<Vec3b> (y-1,x);
            Vec3b magright = tempGrad1.at<Vec3b> (y,x+1);   Vec3b magleft = tempGrad1.at<Vec3b> (y,x-1);

            float dx,dy;
            for(int i=0; i<3; i++)
            {
                dx = (float) (magright[i] - magleft[i]);
                dy = (float) (magup[i] - magdown[i]);
                mag[i] = sqrt((dx*dx) + (dy*dy));
                angle[i] = atan2(dy,dx);
                angle[i] = cvRound((angle[i]*180/PI)+180)/2;
                if(angle[i] > 180 )
                {
                    cout << "angle >180"<< endl;
                    exit(-1);
                }

                angle[i] = floor((angle[i]-(binSize/2))/binSize);
            }

            grad.at<Vec3f> (y-1,x-1) = mag;
            orient.at<Vec3f> (y-1,x-1) = angle;
        }
    }
}



/*!
  @brief Finds BhattacharyaCoeff between two hisrograms.
  @param[in] hist1 First Histogram.
  @param[in] hist2 Second Histogram.
  @param[out] bhatCoefVal Bhattacharya Coeff obtained through histogram matching.
*/
unsigned char computeBhattacharyaCoefficient( Mat hist1, Mat hist2 , float* bhatCoefVal )
{
    (*bhatCoefVal) = 0.0;
    for( int counter = 0 ; counter < hist1.cols ; ++counter )
        (*bhatCoefVal) = (*bhatCoefVal) + sqrt( ( hist1.at<float>(0,counter) ) * ( hist2.at<float>(0, counter) ) );

    return( 0 );
}



/*!
  @brief Mark the agents with their assigned colours.
  @param[in] img Image in which markings to be done.
*/
void MultiTracker::markAgents(Mat img)
{

    if(!dead /*&& activeTimeSpan >1*/)
    {
        if(status == true)
        {
            rectangle(img,trackWindow.tl(), trackWindow.br(), Scalar(blue,green,red), 3);

            char tempText[100];
            sprintf(tempText,"%d",id);
            putText(img,tempText,Point(trackWindow.x,trackWindow.y),2, 0.75,Scalar(blue,green,red),2,LINENAME,false);

            traj.push_back(Point(trackWindow.x+trackWindow.width/2, trackWindow.y+trackWindow.height));

            if(traj.size()>1)
            {
                for(int i=0; i < traj.size()-1; i++)
                    line(img,traj[i+1],traj[i],Scalar(blue,green,red),2);
            }
        }

        if(status==false)
        {
            Rect pred;
            //            pred.x = KF_RectPredicted.x;
            //            pred.y = KF_RectPredicted.y;
            //            pred.width = KF_RectPredicted.width;
            //            pred.height = KF_RectPredicted.height;
            pred = trackWindow;

            checkBoundary(img,pred);

            traj.push_back(Point(pred.x+pred.width/2, pred.y+pred.height));

            if(traj.size()>1)
            {
                for(int i=0; i < traj.size()-1; i++)
                    line(img,traj[i+1],traj[i],Scalar(blue,green,red),2);
            }

            char tempText[100];
            sprintf(tempText,"%d",id);
            putText(img,tempText,Point(pred.x, pred.y),2, 0.75,Scalar(blue,green,red),2,LINENAME,false);

            float x = pred.x+pred.width/2;
            float y = pred.y+pred.height/2;

            if(pred.x+pred.width/2 > img.cols) x = img.cols;
            if(pred.y+pred.height/2 > img.rows) y = img.rows;

            RotatedRect rPred = RotatedRect(Point2f(x,y), Size2f(pred.width,pred.height), 0);
            ellipse(img, rPred, Scalar(blue,green,red), 2, 8);
        }
    }
}


/*!
  @brief Finds Mean Shift Locations for the agent.
  @param[in] frameData Input image for extracting histogram for MS iterations.
  @param[in] bhatCoefThreshold Threshold used for finalizing MS location.
  @param[in,out] matchVal Final match value after iterations.
*/
void MultiTracker::localizeAppearanceByMeanShift(Mat frameData , float bhatCoefThreshold ,float* matchVal )
{

    // Set Mean Shift Tracker Target
    if(rgbHist) setMeanShiftTrackerTarget( hist, trackWindow );

    if(orientHist)  setMeanShiftTrackerTarget( orient_hist, trackWindow );

    if(fourDimHist) setMeanShiftTrackerTarget( fourDim_hist, trackWindow );

    // Set Current Bhattacharya Coefficient to 1.0
    CurrBhatCoeff = 1.0;

    // Tracking to Localize the Agent Region in Current Pixel Set
    meanShiftTrackingRGB( frameData , bhatCoefThreshold );

    // Check, Whether the Current Bhattacharya Coefficient Exceeds the Threshold
    if( CurrBhatCoeff > bhatCoefThreshold )
    {
        // Set the Final Region with Mean-Shift Tracker Output
        MS_Rect.x = CurrRegion.x;
        MS_Rect.y = CurrRegion.y;
        MS_Rect.width = CurrRegion.width;
        MS_Rect.height = CurrRegion.height;

        checkBoundary(frameData,MS_Rect);

        // Updating Histogram Match Value
        (*matchVal) = CurrBhatCoeff;
    }
    // Otherwise,...
    else
    {
        // Set the Final region as the Initial Region
        MS_Rect.x = trackWindow.x;
        MS_Rect.y = trackWindow.y;
        MS_Rect.width = trackWindow.width;
        MS_Rect.height = trackWindow.height;

        checkBoundary(frameData,MS_Rect);

        // Updating Histogram Match Value
        (*matchVal) = CurrBhatCoeff;
    }
}


/*!
  @brief Set the target hidrogram for MS iterations.
  @param[in] targetDensity Matrix in which target histogram is being stored.
  @param[in] initReg Target location required for extracting target histogram.
*/
void MultiTracker:: setMeanShiftTrackerTarget( Mat targetDensity , Rect initReg )
{

    // Set the Data Ready Status
    MeanShiftTrackerDataReady = true;
    // Set the Target Region
    CurrRegion.x = initReg.x;

    CurrRegion.y = initReg.y;
    CurrRegion.width = initReg.width;
    CurrRegion.height = initReg.height;
    PrevRegion.x = initReg.x;
    PrevRegion.y = initReg.y;
    PrevRegion.width = initReg.width;
    PrevRegion.height = initReg.height;

    // Set the Target Histogram
    copyHistogram( targetDensity , TargetHist);
    copyHistogram( TargetHist , CurrPosHist);
    copyHistogram( TargetHist , PrevPosHist);

    // Set Current Bhattacharya Coefficient to 1.0
    CurrBhatCoeff = 1.0;
}


/*!
  @brief Performs original Convergence of MS location.
  @param[in] frameData Input image for extracting histogram for MS iterations.
  @param[in] bhatCoefThreshold Threshold used during iterations.
*/
void MultiTracker::meanShiftTrackingRGB( Mat frameData , float bhatCoeffThreshold )
{
    // If Scale Changes are Zero
    if( ( XScaleChange < EPSILON ) && ( YScaleChange < EPSILON ) )
    {
        // Only Iterate
        iterateRGB(frameData );

        // Enable/Disable Tracker Based on Current Bhattacharya Coefficient
        if( CurrBhatCoeff < bhatCoeffThreshold )
        {
            ToTrack = false;
        }
    }
    // Otherwise, Invoke Scale Updates According to Desired Update-Iterate Order
    else
    {
        // If Iterate-Update is Preferred
        if( UpdateScaleAfterIterations )
        {
            // Iterate and Update
            iterateRGB(frameData );
            updateScaleRGB(frameData );

            // Disable Tracker Based on Current Bhattacharya Coefficient
            if( CurrBhatCoeff < bhatCoeffThreshold )
            {
                ToTrack = false;
            }
        }
        // Otherwise, Execute in Update-Iterate Order
        else
        {
            // Update and Iterate
            updateScaleRGB(frameData );
            iterateRGB(frameData );

            // Disable Tracker Based on Current Bhattacharya Coefficient
            if( CurrBhatCoeff < bhatCoeffThreshold )
            {
                ToTrack = false;
            }
        }
    }
}


/*!
  @brief Performs MS iterations.
  @param[in] frameData Input image for extracting histogram for MS iterations.
*/
void MultiTracker::iterateRGB( Mat frameData )
{
    //    cout << "curr0 rgn" << Mat(CurrRegion.tl()) << Mat(CurrRegion.br()) << endl;

    // Prior to Iterations, Assign Current Region to Previous Region
    PrevRegion.x = CurrRegion.x;
    PrevRegion.y = CurrRegion.y;
    PrevRegion.width = CurrRegion.width;
    PrevRegion.height = CurrRegion.height;

    // Recompute the Position Weighted Color Distribution at Previous Region ( prevRegion )
    // That is Recompute the Previous Position Histogram ( prevPosHist )
    if(rgbHist)
        computeColorDistributionRGB( frameData , PrevRegion , PrevPosHist );

    if(orientHist || fourDimHist)
    {
        MultiTracker tempAgent;
        tempAgent.calcOrientHist(frameData,PrevRegion);

        if(orientHist)
        {
            for(int i=0; i<PrevPosHist.cols; i++)
                PrevPosHist.at<float>(0,i) = tempAgent.orient_hist.at<float>(0,i);
        }

        if(fourDimHist)
        {
            for(int i=0; i<PrevPosHist.cols; i++)
                PrevPosHist.at<float>(0,i) = tempAgent.fourDim_hist.at<float>(0,i);
        }

    }

    // Compute the Bhattacharya Coefficient Between this Histogram and the Target Histogram
    // And Store the Result as Previous Region's Bhattacharya Coefficient ( prevBhatCoeff )
    computeBhattacharyaCoefficient( TargetHist , PrevPosHist , &PrevBhatCoeff );

    // Iterating for Searching Target
    ItrNum = 0;
    float bcMax = -1;
    Rect rectBcMax;
    bool continueIterations = true;
    while( continueIterations && CurrRegion.area() != 0 )
    {
        // Increment Iteration Number
        ItrNum = ( ItrNum ) + 1;

        // Check, Whether Iteration Number Per Frame Exceeds Bound
        if( ItrNum > ItrNumBound )
        {
            // Abnormal Iteration Behavior 1
            ItrCond = 1;
            break;
        }

        // Compute the New Region
        //        cout << "currIter rgn" << Mat(CurrRegion.tl()) << Mat(CurrRegion.br()) << endl;

        getNewRegionRGB(frameData , CurrRegion );

        // Recompute the Position Weighted Color Distribution at Current Region ( currRegion )
        // That is Recompute the Current Position Histogram ( currPosHist )
        if(rgbHist)
            computeColorDistributionRGB( frameData , CurrRegion , CurrPosHist );

        if(orientHist || fourDimHist)
        {
            MultiTracker tempAgent1;
            tempAgent1.calcOrientHist(frameData,CurrRegion);

            if(orientHist)
            {
                for(int i=0; i<CurrPosHist.cols; i++)
                    CurrPosHist.at<float>(0,i) = tempAgent1.orient_hist.at<float>(0,i);
            }

            if(fourDimHist)
            {
                for(int i=0; i<CurrPosHist.cols; i++)
                    CurrPosHist.at<float>(0,i) = tempAgent1.fourDim_hist.at<float>(0,i);
            }

        }

        // Compute the Bhattacharya Coefficient Between this Histogram and the Target Histogram
        // And Store the Result as Current Region's Bhattacharya Coefficient ( currBhatCoeff )
        if(rgbHist)
            computeBhattacharyaCoefficient( TargetHist, CurrPosHist, &CurrBhatCoeff );

        if(CurrBhatCoeff > bcMax)
        {
            bcMax = CurrBhatCoeff;
            rectBcMax = CurrRegion;
        }

        // Fine Tuning of Current Region, if Bhattacharya Coefficient is not Maximised
        int abnItr = 0;
        while( CurrBhatCoeff < PrevBhatCoeff )
        {
            // Increment Sub-Iteration Number
            abnItr = abnItr + 1;

            // Check, Whether Sub-Iterations Per Mean Shift Iteration Exceeds Limit
            if( abnItr > ( SubItrNumBound ) )
            {
                // Abnormal Iteration Behavior 2
                ItrCond = 2;
                break;
            }

            // Try with New Current Region as Dictated by Binary Search
            CurrRegion.y = ( ( CurrRegion.y ) + ( PrevRegion.y ) ) / 2;
            CurrRegion.x = ( ( CurrRegion.x ) + ( PrevRegion.x ) ) / 2;

            // Recompute the Position Weighted Color Distribution at Current Region ( currRegion )
            // That is Recompute the Current Position Histogram ( currPosHist )
            if(rgbHist)
                computeColorDistributionRGB( frameData , CurrRegion , CurrPosHist );

            if(orientHist || fourDimHist)
            {
                MultiTracker tempAgent2;
                tempAgent2.calcOrientHist(frameData,CurrRegion);

                if(orientHist)
                {
                    for(int i=0; i<CurrPosHist.cols; i++)
                        CurrPosHist.at<float>(0,i) = tempAgent2.orient_hist.at<float>(0,i);
                }

                if(fourDimHist)
                {
                    for(int i=0; i<CurrPosHist.cols; i++)
                        CurrPosHist.at<float>(0,i) = tempAgent2.fourDim_hist.at<float>(0,i);
                }

            }

            // Compute the Bhattacharya Coefficient Between this Histogram and the Target Histogram
            // And Store the Result as Current Region's Bhattacharya Coefficient ( currBhatCoeff )
            computeBhattacharyaCoefficient( TargetHist, CurrPosHist, &CurrBhatCoeff );
        }

        // Checking the Stopping Criteria
        int regDist = ( ( PrevRegion.x ) - ( CurrRegion.x ) ) * ( ( PrevRegion.x ) - ( CurrRegion.x ) );
        regDist = regDist + ( ( ( PrevRegion.y ) - ( CurrRegion.y ) ) * ( ( PrevRegion.y ) - ( CurrRegion.y ) ) );

        // Terminate, If Stopping Criteria is Satisfied
        if( regDist <= ( Epsilon ) )
        {
            continueIterations = false;
        }

        // Preparing for Next Iteration : Copying the Current Region to Previous Region
        PrevRegion = CurrRegion;

        // Preparing for Next Iteration : Copying the Current Region Histogram to Previous Region Histogram
        //        memcpy( PrevPosHist.row(1) , CurrPosHist.row(1) , ( ( PrevPosHist.cols ) * sizeof( float ) ) );
        for(int i=0; i<PrevPosHist.cols ; i++)
            PrevPosHist.at<float>(0,i) = CurrPosHist.at<float>(0,i);

        // Preparing for Next Iteration : Copying the Current Bhattacharya Coefficient to the Previous One
        PrevBhatCoeff = CurrBhatCoeff;
    }

    //    cout << "iteration toatl \t \t \t" <<  ItrNum << endl;
    if(ItrNum >= ItrNumBound)
    {
        CurrBhatCoeff = bcMax;
        CurrRegion = rectBcMax;
    }

    // Assert Normal Iterations
    ItrCond = 0;
}


/*!
  @brief Performs MS updations for scale during iterations.
  @param[in] frameData Input image for extracting histogram during MS iterations.
*/
void MultiTracker::updateScaleRGB( Mat frameData )
{
    // Create a Smaller Region By Changing the Scale
    float cx = ( (float) CurrRegion.x ) + ( 0.5 *  ( (float) CurrRegion.width ) );
    float cy = ( (float) CurrRegion.y ) + ( 0.5 *  ( (float) CurrRegion.height ) );
    float sx = 0.5 * ( 1.0 - ( 0.01 * ( XScaleChange ) ) ) * ( (float) CurrRegion.width );
    float sy = 0.5 * ( 1.0 - ( 0.01 * ( YScaleChange ) ) ) * ( (float) CurrRegion.height );
    Rect smallRegion = Rect( cvRound( cx - sx ) , cvRound( cy - sy ) , cvRound( sx + sx ) , cvRound( sy + sy ) );

    checkBoundary(frameData, smallRegion);

    // Computing the Bhattacharya Coeefficient by using the Small Region Histogram Stored in Previous Position's Histogram
    if(rgbHist)
        computeColorDistributionRGB( frameData , smallRegion , PrevPosHist );

    if(orientHist || fourDimHist)
    {
        MultiTracker tempAgent;
        tempAgent.calcOrientHist(frameData,smallRegion);

        if(orientHist)
        {
            for(int i=0; i<PrevPosHist.cols; i++)
                PrevPosHist.at<float>(0,i) = tempAgent.orient_hist.at<float>(0,i);
        }

        if(fourDimHist)
        {
            for(int i=0; i<PrevPosHist.cols; i++)
                PrevPosHist.at<float>(0,i) = tempAgent.fourDim_hist.at<float>(0,i);
        }

    }

    float smallBhatCoeff;
    computeBhattacharyaCoefficient( TargetHist, PrevPosHist, &smallBhatCoeff );

    // Create a Bigger Region By Changing the Scale
    // And Store the Result in the Previous Region
    sx = 0.5 * ( 1.0 + ( 0.01 * ( XScaleChange ) ) ) * ( (float) CurrRegion.width );
    sy = 0.5 * ( 1.0 + ( 0.01 * ( YScaleChange ) ) ) * ( (float) CurrRegion.height );
    Rect bigRegion = Rect( cvRound( cx - sx ) , cvRound( cy - sy ) , cvRound( sx + sx ) , cvRound( sy + sy ) );

    checkBoundary(frameData, bigRegion);

    // Computing the Bhattacharya Coeefficient by using the Big Region Histogram Stored in Previous Position's Histogram
    if(rgbHist)
        computeColorDistributionRGB( frameData , bigRegion , PrevPosHist );

    if(orientHist || fourDimHist)
    {
        MultiTracker tempAgent1;
        tempAgent1.calcOrientHist(frameData,bigRegion);

        if(orientHist)
        {
            for(int i=0; i<PrevPosHist.cols; i++)
                PrevPosHist.at<float>(0,i) = tempAgent1.orient_hist.at<float>(0,i);
        }

        if(fourDimHist)
        {
            for(int i=0; i<PrevPosHist.cols; i++)
                PrevPosHist.at<float>(0,i) = tempAgent1.fourDim_hist.at<float>(0,i);
        }

    }

    float bigBhatCoeff;
    computeBhattacharyaCoefficient( TargetHist, PrevPosHist, &bigBhatCoeff );

    // IF Scaling Down is Better...
    if( ( smallBhatCoeff > ( CurrBhatCoeff ) ) && ( smallBhatCoeff > bigBhatCoeff ) )
    {
        // Adopt Small Region as Current Region
        CurrRegion.x = smallRegion.x;
        CurrRegion.y = smallRegion.y;
        CurrRegion.width = smallRegion.width;
        CurrRegion.height = smallRegion.height;

        checkBoundary(frameData, CurrRegion);

        // Adopt Smaller Region's Bhattacharya Coefficient as Current Region Bhattacharya Coefficient
        CurrBhatCoeff = smallBhatCoeff;
    }

    // IF Scaling Up is Better...
    if( ( bigBhatCoeff > ( CurrBhatCoeff ) ) && ( bigBhatCoeff > smallBhatCoeff ) )
    {
        // Adopt Big Region as Current Region
        CurrRegion.x = bigRegion.x;
        CurrRegion.y = bigRegion.y;
        CurrRegion.width = bigRegion.width;
        CurrRegion.height = bigRegion.height;

        checkBoundary(frameData,CurrRegion);

        // Adopt Smaller Region's Bhattacharya Coefficient as Current Region Bhattacharya Coefficient
        CurrBhatCoeff = bigBhatCoeff;
    }
}



/*!
  @brief Copies histogram from source to destination.
  @param[in] srcHist source Histogram.
  @param[in,out] desHist destination Histogram.
*/
void MultiTracker::copyHistogram( Mat srcHist ,Mat destHist )
{
    for(int i=0; i<srcHist.cols ; i++)
    {
        destHist.at<float>(0,i) = srcHist.at<float>(0,i);
    }
}


/*!
  @brief Finding new Region for next iteration.
  @param[in] frameData Image used for finding new location.
  @param[in,out] newRegion Location for the next iteration.
*/
void MultiTracker::getNewRegionRGB( Mat frameData , Rect&  newRegion )
{
    // Computing Mean Shift Vector
    //    cout << "prev rgn" << Mat(PrevRegion.tl()) << Mat(PrevRegion.br()) << endl;
    float sumWX = 0.0 , sumWY = 0.0 , sumW = 0.0;
    int rightBotX = ( PrevRegion.x ) + ( PrevRegion.width );
    int rightBotY = ( PrevRegion.y ) + ( PrevRegion.height );
    float sx = 0.5 * ( PrevRegion.width );
    float sy = 0.5 * ( PrevRegion.height );
    float cx = ( PrevRegion.x ) + sx;
    float cy = ( PrevRegion.y ) + sy;

    Mat tempGradients, tempOrientations;    //calculating gradients and orientations for histogram calculation for a window
    //    cout << Mat(PrevRegion.tl()) << Mat(PrevRegion.br()) << endl;
    gradImage(frameData, PrevRegion, tempGradients, tempOrientations);

    int tempbins = HIST_BIN_SIZE;

    float invBinSize = ( (float) tempbins ) / 256.0;

    for( int y = ( PrevRegion.y ) ; y < rightBotY ; ++y )
    {
        for( int x = ( PrevRegion.x ) ; x < rightBotX ; ++x )
        {
            int histIndx;

            if(rgbHist)
            {
                Vec3b color = frameData.at< Vec3b >(y,x);

                // Compute the Bin
                int redBin = (int) (invBinSize * ( (float) color[2]));
                int greenBin = (int) (invBinSize * ( (float) color[1]));
                int blueBin = (int) (invBinSize * ( (float) color[0]));

                histIndx = (redBin * pow(tempbins,2) ) + ( greenBin * pow(tempbins,1) ) + (blueBin) ;
            }

            if(orientHist)
            {
                Vec3f orientBin  = tempOrientations.at<Vec3f> (y-PrevRegion.y, x-PrevRegion.x);
                histIndx = (orientBin[2] * pow(orientbins,2) ) + ( orientBin[1]* pow(orientbins,1) ) + (orientBin[0]) ;
            }

            if(fourDimHist)
            {
                Vec3b color = frameData.at< Vec3b >(y,x);
                Vec3f orientBin  = tempOrientations.at<Vec3f> (y-PrevRegion.y, x-PrevRegion.x);

                int max;

                if(orientBin[0]>=orientBin[1] && orientBin[0]>=orientBin[2]) max = orientBin[0];
                if(orientBin[1]>=orientBin[0] && orientBin[1]>=orientBin[2]) max = orientBin[1];
                if(orientBin[2]>=orientBin[0] && orientBin[2]>=orientBin[1]) max = orientBin[2];

                // Compute the Bin
                int redBin = (int) (invBinSize * ( (float) color[2]));
                int greenBin = (int) (invBinSize * ( (float) color[1]));
                int blueBin = (int) (invBinSize * ( (float) color[0]));

                histIndx = (redBin * pow(tempbins,2) ) + ( greenBin * pow(tempbins,1) ) /*+ (blueBin*pow(tempbins,1)) */+ max ;
            }

            double weightVal = 0;
            if(PrevPosHist.at<float>(0,histIndx) != 0)
                weightVal = sqrt( ( TargetHist.at<float>(0,histIndx )) / ( PrevPosHist.at<float>(0,histIndx) ) );

            //           cout << TargetHist.at<float>(0,histIndx ) <<"\t" << PrevPosHist.at<float>(0,histIndx) << endl;
            sumWY = sumWY + ( y * weightVal );
            sumWX = sumWX + ( x * weightVal );
            sumW = sumW + weightVal;
        }
    }

    cx = sumWX / sumW;
    cy = sumWY / sumW;
    newRegion.x = cvRound( cx - sx );
    newRegion.y = cvRound( cy - sy );
    //    newRegion.width = PrevRegion.width;
    //    newRegion.height = PrevRegion.height;

    checkBoundary(frameData,newRegion);
}


/*!
  @brief Finds colour distribution of given location in the image.
  @param[in] frameData Input image for histogram extraction.
  @param[in] reg rectangle to location in the image.
  @param[in] hist Histogram to be calculated for the specified rectangle in the image.
*/
void getHist(Mat frameData, Rect reg, Mat& hist, int bins)
{
    if(reg.area() == 0)
    {
        reg.x = 0;
        reg.y = 0;
        reg.width = frameData.cols;
        reg.height = frameData.rows;
    }
    //    reg.x += cvRound(reg.width*0.2);
    //    reg.width = cvRound(reg.width*0.6);
    //    reg.y += cvRound(reg.height*0.1);
    //    reg.height = cvRound(reg.height*0.8);

    hist = Mat(1, pow(bins,3), CV_32FC1, Scalar(0));

    // Computing the Right Bottom of the Region
    int rightBotX = ( reg.x ) + ( reg.width );
    int rightBotY = ( reg.y ) + ( reg.height );

    // Computing Color Distribution from the Rectangular/Elliptical Region
    float sumW = 0.0;
    float invBinSize = ( (float) ( bins ) ) / 256.0;
    int binSqr = ( bins ) * ( bins );

    for( int y = ( reg.y ) ; y < rightBotY ; ++y )
    {
        for( int x = ( reg.x ) ; x < rightBotX ; ++x )
        {
            Vec3b color = frameData.at< Vec3b >(y,x);

            // Compute the Bin
            int redBin = (int) (invBinSize * ( (unsigned char) color[2]));
            int greenBin = (int) (invBinSize * ( (unsigned char) color[1]));
            int blueBin = (int) (invBinSize * ( (unsigned char) color[0]));

            // Compute the Histogram Index
            int histIndx = (redBin * binSqr ) + ( greenBin * bins ) + blueBin;

            // Update Histogram and Weight Sum
            hist.at<float>(0,histIndx ) = hist.at<float>(0,histIndx ) + 1;

            sumW = sumW + 1;
        }
    }

    // Normalizing the Histogram
    if( sumW > EPSILON )
    {
        float inv_sumW = 1.0 / sumW;
        for( int histIndx = 0 ; histIndx < hist.cols  ; ++histIndx )
        {
            hist.at<float>(0, histIndx) = inv_sumW * ( hist.at<float>(0,histIndx ) );
        }
    }
}

/*!
  @brief Validates rectangle and fixes its dimensions inside the image by snipping the rectangle.
  @param[in] frameData Image used for fixing rectangle dimensions.
  @param[in,out] tempRect Rectangle whose dimensions are to be fixed.
*/
void checkBoundary(Mat frameData, Rect& tempRect)
{
    if(tempRect.x < 0)
    {
        tempRect.width  =  tempRect.width + tempRect.x ;
        tempRect.x =0 ;
        if(tempRect.width < 0)
        {
            tempRect.width = 0;
            tempRect.height = 0;
        }
    }

    if(tempRect.y < 0)
    {
        tempRect.height =  tempRect.height + tempRect.y ;
        tempRect.y = 0;
        if(tempRect.height < 0)
        {
            tempRect.height = 0;
            tempRect.width = 0;
        }
    }

    if(tempRect.x > frameData.cols) tempRect.x = frameData.cols ;
    if(tempRect.y > frameData.rows) tempRect.y = frameData.rows;

    int rightBotX = tempRect.x + tempRect.width;
    int rightBotY = tempRect.y + tempRect.height;

    if(rightBotY  >  frameData.rows )  tempRect.height  =  frameData.rows - tempRect.y ;
    if(rightBotX  >  frameData.cols)   tempRect.width  =  frameData.cols - tempRect.x ;
}


/*!
  @brief Calculate overlap between twoo rectangles.
  @param[in] tl1 Topleft point of first rectangle.
  @param[in] tl2 Topleft point of second rectangle.
  @param[in] sz1 Size of first rectangle.
  @param[in] sz2 Size of second rectangle.
  @param[in,out] overlap Overlap calculated between two rectangles.
*/
void calcOverlap(Point tl1, Point tl2, Size sz1, Size sz2, float *overlap)
{
    Rect roi;

    int x_tl = max(tl1.x, tl2.x);
    int y_tl = max(tl1.y, tl2.y);
    int x_br = min(tl1.x + sz1.width, tl2.x + sz2.width);
    int y_br = min(tl1.y + sz1.height, tl2.y + sz2.height);
    if (x_tl < x_br && y_tl < y_br)
        roi = Rect(x_tl, y_tl, x_br - x_tl, y_br - y_tl);
    else
        roi = Rect(0,0,0,0);

    *overlap = roi.width*roi.height;
}

/*!
  @brief Small version of above function.
  */
float findOverlap(Rect r1, Rect r2)
{
    float overlap = 0;
    Point tl1 = r1.tl();
    Point tl2 = r2.tl();
    Size sz1 =r1.size();
    Size sz2 = r2.size();

    Rect roi;

    int x_tl = max(tl1.x, tl2.x);
    int y_tl = max(tl1.y, tl2.y);
    int x_br = min(tl1.x + sz1.width, tl2.x + sz2.width);
    int y_br = min(tl1.y + sz1.height, tl2.y + sz2.height);
    if (x_tl < x_br && y_tl < y_br)
        roi = Rect(x_tl, y_tl, x_br - x_tl, y_br - y_tl);
    else
        roi = Rect(0,0,0,0);

    overlap = roi.width*roi.height;

    return overlap;
}

bool verifyResultantRoi(cv::Rect r1, cv::Rect r2, float& ARerr, float& scale, float& roiConf, bool rotateFlag = false)
{
    float ARnow = 0;
    scale = 0;

    if(!rotateFlag)
    {
        ARnow = float(r2.width) / float(r2.height);
        scale = float(r2.width) / float(r1.width);
    }
    else
    {
        ARnow = float(r2.height) / float(r2.width);
        scale = float(r2.height) / float(r1.width);
    }

    float AR = float(r1.width) / float(r1.height);

    ARerr = AR/ARnow;

    //    cout << "err\t" << ARerr << "\t" << scale << endl;
    //    if(ARerr > 1.3 || ARerr < 0.7)
    //    {
    //        return false;
    //    }

    //    if(scale < 0.67 || scale > 1.5 )
    //    {
    //        return false;
    //    }


    float scaleConf = float(r1.area())/float(r2.area());
    scaleConf = scaleConf > 1 ? 1.0/scaleConf : scaleConf;

    float ARConf = ARerr > 1 ? 1.0/ARerr : ARerr;

    //    cout << ARnow << "\t" << ARerr << "\t" <<scale << endl;
    //    cout << scaleConf << "\t" << ARConf << endl;
    if(scaleConf > 0.7 && ARConf > 0.7)
    {
        roiConf = 0.5 * (scaleConf + ARConf);
        return true;
    }
    else
        return false;
}


void crossCheckMatching( Ptr<DescriptorMatcher>& descriptorMatcher,
                         const Mat& descriptors1, const Mat& descriptors2,
                         vector<DMatch>& filteredMatches12, int knn=1 )
{
    filteredMatches12.clear();

    vector<vector<DMatch> > matches12, matches21;
    descriptorMatcher->knnMatch( descriptors1, descriptors2, matches12, knn );
    descriptorMatcher->knnMatch( descriptors2, descriptors1, matches21, knn );
    for( size_t m = 0; m < matches12.size(); m++ )
    {
        bool findCrossCheck = false;
        for( size_t fk = 0; fk < matches12[m].size(); fk++ )
        {
            DMatch forward = matches12[m][fk];

            for( size_t bk = 0; bk < matches21[forward.trainIdx].size(); bk++ )
            {
                DMatch backward = matches21[forward.trainIdx][bk];
                if( backward.trainIdx == forward.queryIdx )
                {
                    filteredMatches12.push_back(forward);
                    findCrossCheck = true;
                    break;
                }
            }
            if( findCrossCheck ) break;
        }
    }
}



double surfDesc_Matching1(Mat img1, Mat img2,  vector<KeyPoint> temp1Keypoint, vector<KeyPoint> temp2Keypoint,
                          Mat temp1Desc, Mat temp2Desc, Point2f& centerSURF, cv::Rect& roiOut, Mat& drawImg)
{
    if(temp1Desc.empty() || temp2Desc.empty())
    {
        //        cerr << "Descriptors size is zero" << endl;
        return 0;
    }

    vector<int> queryIdxs,  trainIdxs;
    vector<DMatch> filteredMatches;

    Ptr<DescriptorMatcher> descriptorMatcher;
    descriptorMatcher = DescriptorMatcher::create( "FlannBased" );


    crossCheckMatching( descriptorMatcher, temp1Desc, temp2Desc, filteredMatches, 1 );

    for( size_t i = 0; i < filteredMatches.size(); i++ )
    {
        queryIdxs.push_back(filteredMatches[i].queryIdx);
        trainIdxs.push_back(filteredMatches[i].trainIdx);
    }

    Mat pointsTransed; // Points for inverse homography
    Mat H12;

    if( RANSAC_THREHOLD >= 0 )
    {
        vector<Point2f> points1; KeyPoint::convert(temp1Keypoint, points1, queryIdxs);
        vector<Point2f> points2; KeyPoint::convert(temp2Keypoint, points2, trainIdxs);
        if (points2.size() < 4 )
            return 0;
        H12 = findHomography( Mat(points1), Mat(points2), cv::RANSAC, RANSAC_THREHOLD );
    }

    //    Mat drawImg;

    double percentage_match = 0.0;

    if( !H12.empty() )
    {
        vector<char> matchesMask( filteredMatches.size(), 0 );
        vector<Point2f> points1; KeyPoint::convert(temp1Keypoint, points1, queryIdxs);
        vector<Point2f> points2; KeyPoint::convert(temp2Keypoint, points2, trainIdxs);

        Mat points1t; perspectiveTransform(Mat(points1), points1t, H12);


        int count = 0;
        for( size_t i1 = 0; i1 < points1.size(); i1++ )
        {
            if( ( norm(points2[i1] - points1t.at<Point2f>((int)i1,0)) <= 5 ))
            {
                count++;
                matchesMask[i1] = 1;
            }
        }

        vector<Point2f> centerVec;
        Mat centerVecOut;
        centerVec.push_back(centerSURF);
        centerVec.push_back(cv::Point2f(0,0));
        centerVec.push_back(cv::Point2f(img1.cols,img1.rows));

        perspectiveTransform((Mat)centerVec, centerVecOut, H12);

        centerSURF = centerVecOut.at<Point2f>(0);
        roiOut.x = centerVecOut.at<cv::Point2f>(1).x;
        roiOut.y = centerVecOut.at<cv::Point2f>(1).y;
        roiOut.width = abs(centerVecOut.at<cv::Point2f>(2).x - centerVecOut.at<cv::Point2f>(1).x);
        roiOut.height = abs(centerVecOut.at<cv::Point2f>(2).y - centerVecOut.at<cv::Point2f>(1).y);

        percentage_match = (double)(count*100)/(temp2Desc.rows+temp1Desc.rows-count);
        points1.clear();points2.clear();
        drawMatches( img1, temp1Keypoint, img2, temp2Keypoint, filteredMatches, drawImg, Scalar(0, 255, 0), Scalar(0, 0, 255), matchesMask);

        //    namedWindow("cores",WINDOW_NORMAL);
        //    imshow("cores",drawImg);
        //    waitKey(0);
    }

    return percentage_match;

}

/*!
  @brief Finds the best matching image ROI with a source template image using SURF point matching.
  @param[in] imgSrc                 Input image template to match with.
  @param[in] imgTarget              Input target image to resolve for target candidates.
  @param[in] roiTargetCandidates    The ROIs to find best match among all.
  @param[out] roiOut                The selected ROI.
  @param[out] roiConf               The confidence value for selected ROI.
  @param[out] testOK                Flag to specify if matching succeeded or failed.
  @retval                           The index of selected ROI.
*/
int resolveTargetSurf(Mat imgSrc, Mat imgTarget, vector<Rect>& roiTargetCandidates, Rect& roiOut, float& roiConf, bool& testOK)
{
    vector<Rect> targetRects;
    Mat img1 = imgSrc.clone();

    int imgArea1 = img1.rows * img1.cols;
    int heshThresh1 = 0;
    //    heshThresh1 = imgArea1 < 10000 ? 0 : float(imgArea1)/300.0;

    SURF* mySurf = new SURF;
    mySurf->set("hessianThreshold",heshThresh1);

    vector<KeyPoint> kp1;
    mySurf->detect(img1,kp1);
    Mat desc1;
    mySurf->compute(img1,kp1,desc1);


    vector<double> percentMatches;
    float maxMatch = 0;
    int maxMatchIndex = -1;

    for(int k=0; k<roiTargetCandidates.size(); k++)
    {
        Mat img2 = imgTarget(roiTargetCandidates[k]).clone();

        vector<KeyPoint> kp2;
        Mat desc2;
        int imgArea2 = img2.rows * img2.cols;
        int heshThresh2 = 0;
        //        heshThresh2 = imgArea2 < 10000 ? 0 : float(imgArea2)/300.0;
        mySurf->set("hessianThreshold",heshThresh2);
        mySurf->detect(img2,kp2);
        mySurf->compute(img2,kp2,desc2);

        Point2f center;
        center.x = roiTargetCandidates[k].x + roiTargetCandidates[k].width/2;
        center.y = roiTargetCandidates[k].y + roiTargetCandidates[k].height/2;

        Mat imgDraw;
        roiOut;
        double percentMatch = surfDesc_Matching1(img2,img1,kp2,kp1,desc2,desc1,center,roiOut,imgDraw);

        Rect roiIn = Rect(0,0,img1.cols,img1.rows);
        float ARerr=0,scale=0;
        testOK = verifyResultantRoi(roiIn,roiOut,ARerr,scale,roiConf);

        if(percentMatch > maxMatch && testOK)
        {
            maxMatch = percentMatch;
            maxMatchIndex = k;
        }

        Rect targetRect(0,0,0,0);

        if(percentMatch > 20 && testOK)
        {
            targetRect.width = roiTargetCandidates[k].width;
            targetRect.height = roiTargetCandidates[k].height;
            targetRect.x = center.x - targetRect.width/2 ;//+ roiSrc.x;
            targetRect.y = center.y - targetRect.height/2 ;//+ roiSrc.y;
        }

        percentMatches.push_back(percentMatch);
        targetRects.push_back(targetRect);

        //        cout << "percent match \t" << percentMatch << "\t" << testOK << endl;

        //            char textString[100];
        //            sprintf(textString,"%d.jpg",k+1);
        //            imwrite(textString,imgDraw);
    }

    roiTargetCandidates.clear();
    for(int k=0; k<targetRects.size(); k++)
        roiTargetCandidates.push_back(targetRects[k]);

    return maxMatchIndex;
}


void getImageParts(Mat img1, vector<Rect> detections, vector<Rect>& scanRegions)
{
    scanRegions.clear();

    if(detections.empty())
    {
        Rect region = Rect(0,0,img1.cols,img1.rows);

        scanRegions.push_back(region);
    }
    else
    {
        vector<int> rectX;
        for(int k=0; k<detections.size(); k++)
        {
            rectX.push_back(detections[k].x);
            //        rectangle(img1,detections[k],Scalar(0,255,0),2,LINENAME);
        }

        vector<int> sortedIdx;
        sortIdx(rectX,sortedIdx,0);


        for(int k=0; k<detections.size()+1; k++)
        {
            int startX = (k == 0 ? 0 : detections[sortedIdx[k-1]].br().x);
            int endX = (k == detections.size() ? img1.cols : detections[sortedIdx[k]].x);


            int width = endX - startX;

            if(width <= 0)
                continue;

            Rect region = Rect(startX,0,width,img1.rows);

            scanRegions.push_back(region);
        }

        for(int k=0; k<scanRegions.size(); k++)
        {
            //        rectangle(img1,scanRegions[k],Scalar(0,0,255),2,LINENAME);
        }


        //            imshow("imgParts",img1);
        //            waitKey(0);
    }
}

/*!
  @brief Finds a random color for a given seed, defualt seed id current timestamp in nanoseconds.
  @param[in]    Input seed for random number generator.
  @return       Random Scalar color value.
  */
Scalar getRandColor(/*uint64_t seed = ros::Time::now().toNSec()*/)
{
    timespec t1;
    clock_gettime(0,&t1);
    RNG randNum(t1.tv_nsec);
    //    RNG randNum(seed);
    Scalar randColor;

    int t = randNum.uniform(0,256);
    for(int k=0; k<3; k++)
    {
        t = randNum.uniform(0,256);
        randColor[k] = t;
    }

    return randColor;
}

/*!
  @brief Finds the delaunay triangulation and then Euclidean Minimum Spanning Tree using Prim's algorithm. Marks the graph on image.
  @param[in]    img         Image to draw the graph.
  @param[in]    points1     Input points on which the graph/tree has to made.
  @param[out]   adjMat      Adjacency Matrix for the graph. Value is the index of the edge. -1 if no edge exists for the pair.
  @param[out]   nbrList     List of neighbours for the given points in form of a vector. It needs to be inititated with points size.
  @param[in]    EMST        Set to true if Euclidean Minimum Spanning Tree is required, else Delaunay Trainagulation is done.
  */
void draw_subdiv( Mat img, vector<Point2f> points1, Mat_<int>& adjMat, vector< vector<int> >& nbrList, bool EMST = false )
{
    if(points1.size() < 2)
    {
        //        cerr << "Not enough points for drawing Delaunay Traingualation" << endl;
        return;
    }

    int counter = 0;

    adjMat = Mat_<int>(points1.size(),points1.size(),-1);

    Rect rect = Rect(0,0,img.cols*1.2,img.rows*1.2);
    Subdiv2D subdiv = Subdiv2D(rect);

    subdiv.insert(points1);

#if 0
    vector<Vec6f> triangleList;
    subdiv.getTriangleList(triangleList);
    vector<Point> pt(3);

    for( size_t i = 0; i < triangleList.size(); i++ )
    {
        Vec6f t = triangleList[i];
        pt[0] = Point(cvRound(t[0]), cvRound(t[1]));
        pt[1] = Point(cvRound(t[2]), cvRound(t[3]));
        pt[2] = Point(cvRound(t[4]), cvRound(t[5]));
        line(img, pt[0], pt[1], delaunay_color, 1, LINENAME, 0);
        line(img, pt[1], pt[2], delaunay_color, 1, LINENAME, 0);
        line(img, pt[2], pt[0], delaunay_color, 1, LINENAME, 0);
    }
#else
    vector<Vec4f> edgeList;
    vector<float> edgeLength;
    vector<Point2f> edgePoints;
    subdiv.getEdgeList(edgeList);
    for( size_t i = 0; i < edgeList.size(); i++ )
    {
        Vec4f e = edgeList[i];
        Point pt0 = Point(cvRound(e[0]), cvRound(e[1]));
        Point pt1 = Point(cvRound(e[2]), cvRound(e[3]));
        if(e[0] < 0 || e[0] > img.cols || e[1] < 0 || e[1] > img.rows || e[2] < 0 || e[2] > img.cols || e[3] < 0 || e[3] > img.rows)
            continue;
        //        if(norm(pt0-pt1) < 50)
        edgeLength.push_back(norm(pt0-pt1));
        edgePoints.push_back(Point2f(e[0],e[1]));
        edgePoints.push_back(Point2f(e[2],e[3]));

        if(!EMST)
        {
            int vtx1, vtx2, eg;
            subdiv.locate(pt0,eg,vtx1);
            subdiv.locate(pt1,eg,vtx2);

            adjMat(vtx1-4, vtx2-4) = counter;
            adjMat(vtx2-4, vtx1-4) = counter;

            nbrList[vtx1-4].push_back(vtx2-4);
            nbrList[vtx2-4].push_back(vtx1-4);

            counter++;

            line(img, pt0, pt1, Scalar(255,255,255), 2, LINENAME, 0);
        }
    }
#endif


    if(EMST)
    {
        vector<int> sortedEdgeIdx;
        sortIdx(edgeLength,sortedEdgeIdx,CV_SORT_ASCENDING);

        //**********************************************************************************************************
        // Prim's Algorithm for EMST
        vector< vector< int > > vertexEdges(points1.size());
        vector< vector< float > > vertexEdgeLength(points1.size());

        // Store all the Delaunay neoghbors in a sorted order along with the corresponding edgeLengths
        for(uint i=0; i<edgeLength.size(); i++)
        {
            int vtx1, vtx2, eg;
            subdiv.locate(edgePoints[2*sortedEdgeIdx[i]],eg,vtx1);
            subdiv.locate(edgePoints[2*sortedEdgeIdx[i]+1],eg,vtx2);

            vertexEdges[vtx1-4].push_back(vtx2-4);
            vertexEdges[vtx2-4].push_back(vtx1-4);

            vertexEdgeLength[vtx1-4].push_back(edgeLength[sortedEdgeIdx[i]]);
            vertexEdgeLength[vtx2-4].push_back(edgeLength[sortedEdgeIdx[i]]);
        }

        // Stores the vertices that have been parsed
        // and search for a minimum weight edge is done in the neighbors of these only
        vector<int> vertexCounter;
        vertexCounter.push_back(0); // Choose a vertex arbitrarily


        // Mark the vertices that have been parsed
        vector<int> vertexDone(points1.size(),0);
        vertexDone[0] = 1;

        while(vertexCounter.size() < points1.size())
        {
            float minVal = 9999;
            Point2i minValIdx;

            // Loop over all the traversed vertices
            for(uint j=0; j<vertexCounter.size(); j++)
            {
                // Loop over all the neighbors of the traversed vertex
                for(uint k=0; k<vertexEdges[vertexCounter[j]].size(); k++)
                {
                    if(!vertexDone[vertexEdges[vertexCounter[j]][k]])
                    {
                        if(vertexEdgeLength[vertexCounter[j]][k] < minVal)
                        {
                            minVal = vertexEdgeLength[vertexCounter[j]][k];
                            minValIdx = Point(vertexEdges[vertexCounter[j]][k], vertexCounter[j]);
                        }
                        break;
                    }
                }
            }

            vertexCounter.push_back(minValIdx.x);
            vertexDone[minValIdx.x] = 1;

            adjMat(minValIdx.y, minValIdx.x) = counter;
            adjMat(minValIdx.x, minValIdx.y) = counter;

            nbrList[minValIdx.y].push_back(minValIdx.x);
            nbrList[minValIdx.x].push_back(minValIdx.y);

            counter ++;

            line(img,points1[minValIdx.x], points1[minValIdx.y], Scalar(0,0,255), 2, LINENAME);

        }
    }
    //**********************************************************************************************************
}


/*!
  @brief Adaptive K-Means clustering of the data.
  @param[in]    input               Input Samples to be clustered
  @param[in]    threshold_radius    Radius Threshold for clustering
  @param[out]   clusterCenters      Cluster Centers.
  @param[out]   labels              Cluster labels for the input data
  @param[out]   clusterData         Clustered data stored in form of a vector.
  */
void adaptiveKmeans(Mat input, int threshold_radius, Mat& clusterCenters, Mat& labels, vector< vector<Mat> >& clusterData)
{
    int noc=0;

    // For all the points in the sample set
    for(unsigned int iter=0;iter<input.rows;iter++)
    {
        int label=0;

        Mat p1 = input.row(iter);

        float minDistance=999;

        // Find minimum distance of the given point from the available cluster centers
        for(int j=0; j<noc; j++)
        {

            Mat p2 = clusterCenters.row(j);

            float distance = norm(p1-p2);

            if(distance<minDistance)
            {
                minDistance=distance;
                label=j;
            }

        }

        // If the minimum distance is in range of the radius threshold
        if(minDistance<threshold_radius)
        {
            // Update the data
            labels.push_back(label);
            clusterData[label].push_back(p1);

            int clusterMemberCount = clusterData[label].size();

            Mat sumVal = Mat::zeros(1,p1.cols,CV_32FC1);

            for(int k1=0; k1<clusterMemberCount; k1++)
            {
                sumVal += clusterData[label][k1];
            }

            sumVal /= clusterMemberCount;

            clusterCenters.row(label) = sumVal.clone();
        }
        // Else create a new cluster
        else
        {
            noc++;
            labels.push_back(noc);

            vector<Mat> temp;
            temp.push_back(p1);
            clusterData.push_back(temp);

            clusterCenters.push_back(p1);
        }
    }
}


/*!
@brief Scales a Rectangle times the given value (1.1 (expansion) or 0.6 (contraction) say). Changes the input Rectangle itself.
  */
void scaleRect(Rect& ioRoi, float percentScale)
{
    Point2f center = 0.5 * (ioRoi.tl() + ioRoi.br());

    ioRoi.width = float(ioRoi.width) * percentScale;
    ioRoi.height = float(ioRoi.height) * percentScale;

    ioRoi.x = center.x - ioRoi.width/2;
    ioRoi.y = center.y - ioRoi.height/2;
}



/*!
  @brief Computes histogram of an image without counting the pixels of color value as (0,0,0).
  */
void calcHistogram(Mat srcImg, Mat& hist, int bins=HIST_BIN_SIZE )
{
    hist = Mat(1, pow(bins,3), CV_32FC1, Scalar::all(0));

    // Computing Color Distribution from the Rectangular/Elliptical Region
    float sumW = 0.0;
    float invBinSize = ( (float) ( bins ) ) / 256.0;
    int binSqr = ( bins ) * ( bins );

    for(int i=0; i<srcImg.rows; i++)
    {
        for(int j=0; j<srcImg.cols; j++)
        {
            Vec3b color = srcImg.at<Vec3b>(i,j);

            if(color == Vec3b(0,0,0))
                continue;

            // Compute the Bin
            int redBin = (int) (invBinSize * ( (unsigned char) color[2]));
            int greenBin = (int) (invBinSize * ( (unsigned char) color[1]));
            int blueBin = (int) (invBinSize * ( (unsigned char) color[0]));

            // Compute the Histogram Index
            int histIndx = (redBin * binSqr ) + ( greenBin * bins ) + blueBin;

            // Update Histogram and Weight Sum
            hist.at<float>(0,histIndx ) = hist.at<float>(0,histIndx ) + 1;

            sumW = sumW + 1;
        }
    }

    // Normalizing the Histogram
    if( sumW > 0 )
    {
        float inv_sumW = 1.0 / sumW;
        for( int histIndx = 0 ; histIndx < hist.cols  ; ++histIndx )
        {
            hist.at<float>(0, histIndx) = inv_sumW * ( hist.at<float>(0,histIndx ) );
        }
    }
}

/*!
  @brief Attaches two images horizontally as default, set the flag false to append vertically. Images don't need to be of same size,
    but same type.
  */
void attachImages(Mat img1, Mat img2, Mat& imgOut, bool horizontal = true)
{
    assert(img1.type() == img2.type());

    if(horizontal)
    {
        int numRows = img1.rows > img2.rows ? img1.rows : img2.rows;
        int numCols = img1.cols + img2.cols;

        imgOut = Mat::zeros(numRows,numCols,img1.type());

        img1.copyTo(imgOut.rowRange(0,img1.rows).colRange(0,img1.cols));
        img2.copyTo(imgOut.rowRange(0,img2.rows).colRange(img1.cols,numCols));
    }
    else
    {
        int numCols = img1.cols > img2.cols ? img1.cols : img2.cols;
        int numRows = img1.rows + img2.rows;

        imgOut = Mat::zeros(numRows,numCols,img1.type());

        img1.copyTo(imgOut.rowRange(0,img1.rows).colRange(0,img1.cols));
        img2.copyTo(imgOut.rowRange(img1.rows,numRows).colRange(0,img2.cols));
    }
}


// Needs to be revisited
void filterDetections(Mat img1, vector<Rect>& detROIs, bool minAreaFlag = false)
{
    vector<Rect> filteredROIs;

    for(int i=0; i<detROIs.size(); i++)
    {
        bool dontAdd = false;

        Rect r1 = detROIs[i];
        for(int j=i+1; j<detROIs.size(); j++)
        {
            Rect r2 = detROIs[j];

            float overlap = 0;
            calcOverlap(r1.tl(),r2.tl(),r1.size(),r2.size(),&overlap);
            float minArea;
            if(minAreaFlag)
                minArea = r1.area() < r2.area() ? r1.area() : r2.area();
            else
                minArea = r1.area();

            overlap = overlap/float(minArea);

            if(overlap > 0.5)
            {
                //                MultiTracker tempTracker;
                //                Mat hist1, hist2;
                //                tempTracker.computeColorDistributionRGB(img1,r1,tempTracker.hist);
                //                hist1 = tempTracker.hist.clone();
                //                tempTracker.computeColorDistributionRGB(img1,r2,tempTracker.hist);
                //                hist2 = tempTracker.hist.clone();

                //                float matchVal = 0;
                //                computeBhattacharyaCoefficient(hist1,hist2,&matchVal);

                //                if(matchVal > 0.8)
                //                {
                dontAdd = true;
                //                }
            }
        }

        if(!dontAdd)
            filteredROIs.push_back(detROIs[i]);
    }

    detROIs.clear();
    for(int i=0; i<filteredROIs.size(); i++)
        detROIs.push_back(filteredROIs[i]);
}


/*!
  @brief Finds the most overlapping window for the given window in an image.
  */
void findMostOverlappingWindow(Rect r1, vector<Rect> windows, int& maxOverlapIndex, float& maxOverlap, int skipIndex = -1)
{
    maxOverlap = 0;
    maxOverlapIndex = -1;
    for(int i1=0; i1<windows.size(); i1++)
    {
        if(skipIndex == i1)
            continue;

        Rect r2 = windows[i1];
        float overlap = 0;

        calcOverlap(r1.tl(),r2.tl(),r1.size(),r2.size(),&overlap);
        float minArea = r1.area() < r2.area() ? r1.area() : r2.area();
        overlap = overlap/minArea;

        if(overlap > maxOverlap)
        {
            maxOverlap = overlap;
            maxOverlapIndex = i1;
        }
    }
}

/*!
  @brief Track the target locally using SURF features
  */
void track_SURF(Mat img, MultiTracker tracker, Rect& surfRoiOut)
{
    SURF* mySurf;

    Mat surfSrcImg = tracker.agentImgUpdated;
    Rect r1(0,0,surfSrcImg.cols,surfSrcImg.rows);
    vector<KeyPoint> kp1;
    Mat desc1;

    mySurf->detect(surfSrcImg,kp1);
    mySurf->compute(surfSrcImg,kp1,desc1);

    Rect r2 = tracker.KF_RectPredicted;
    Mat surfDstImg = img(r2);
    vector<KeyPoint> kp2;
    Mat desc2;

    mySurf->detect(surfDstImg,kp2);
    mySurf->compute(surfDstImg,kp2,desc2);

    Point2f surfCenter = 0.5 * (r1.tl() + r1.br());
    Mat surfImgDraw;
    double surfPercentMatch = surfDesc_Matching1(surfSrcImg,surfDstImg,kp1,kp2,desc1,desc2,surfCenter,
                                                 surfRoiOut,surfImgDraw);

    float surfARerr=0,surfScale=0,roiConf=0;
    bool surfTestOK = verifyResultantRoi(r1,surfRoiOut,surfARerr,surfScale,roiConf);

    if(!surfTestOK || surfPercentMatch < 30)
    {
        surfPercentMatch = 0;
        surfRoiOut = Rect(0,0,0,0);
    }
    else if(surfPercentMatch > 30)
    {
        surfRoiOut.x += r2.x;
        surfRoiOut.y += r2.y;
    }
}

/*!
  @brief Predicts the object location in the image.
  @param[in,out] tracker   The tracker object.
  @param[in] frameNum      Frame Number.
*/
void predictAgent(MultiTracker& tracker, int frameNum)
{
    // If no initial prediction was set using SURF matching or other method, predict using Kalman Filter.
    if(tracker.initialPrediction.area() == 0)
    {
        setIdentity(tracker.KF_CV.measurementMatrix,Scalar::all(0));
        Mat measure = Mat::zeros(2, 1, CV_32F);
        measure.convertTo(measure,CV_32FC1);
        tracker.KF_CV.correct(measure);

        tracker.KF_CV.predict();

        tracker.KF_RectPredicted.x = tracker.KF_CV.statePost.at<float>(0)-tracker.KF_RectPredicted.width/2;
        tracker.KF_RectPredicted.y = tracker.KF_CV.statePost.at<float>(1)-tracker.KF_RectPredicted.height/2;

        tracker.KF_RectPredicted.width = tracker.trackWindow.width;
        tracker.KF_RectPredicted.height = tracker.trackWindow.height;

        tracker.trackWindow = tracker.KF_RectPredicted;

        tracker.exitTimeSpan++;
    }
    // Assign the initial prediction
    else
    {
        tracker.trackWindow = tracker.initialPrediction;
    }

    if(tracker.agentState != 5)
    {
        tracker.frameIds.push_back(frameNum);
        tracker.frameROIs.push_back(tracker.KF_RectPredicted);
        tracker.frameObservationFlag.push_back(0);
    }
    tracker.initialPrediction = Rect(0,0,0,0);
}


/*!
  @brief Finds factors edge potential for graph matching.
  */
void getFactors_TS3(ofstream& factorFile,Mat_<int> adjMat,vector<Point2f> points1, vector<Point2f> points2,
                    vector<Mat> hists1, vector<Mat> hists2, vector<Rect> rects1, vector<Rect> rects2,
                    vector<int> dummyFlag, vector<int> dummyVarIndex, vector< Mat_<float> >& edgePotentials)
{
    // Iterate over the adjacency matrix (upper triangualar part only)
    for(uint i=0; i<adjMat.rows; i++)
        for(uint j=i+1; j<adjMat.cols; j++)
        {
            // only if there is a connection between two nodes
            if(adjMat[i][j]!=-1)
            {
                Mat_<float> truthTable(points2.size(),points2.size(),0.0);
                // for this particular case, the required angle between nodes is -90

                // Find the angle between source nodes
                float theta1 = float(180 * 7.0/22.0) *
                        //                        ( atan2( (points1[i].y - points1[j].y), (points1[i].x - points1[j].x) ) );
                        ( atan( (points1[i].y - points1[j].y) / (points1[i].x - points1[j].x) ) );

                bool right = false; // to determine if j is in left or right of i
                if(points1[j].x > points1[i].x)
                    right = true;

                if(isnan(theta1))
                    theta1 = -90;

                float r1 = norm(points1[i]-points1[j]);

                factorFile << 2 << endl; // Number of variables in the factor
                factorFile << i << " " << j << endl; // Indicies of those variables
                factorFile << points2.size() << " " << points2.size() << endl; // Cardinality of each variable

                int factorCounter=0;
                vector<float> nonZeroStateVal;
                vector<int> nonZeroStateIndex;

                // Some factors might have low values for all the target candidates combinations
                float allZeroStateVal;
                int allZeroStateIndex;

                // Iterate over all the possible candidates for given pair of nodes
                for(int m=0; m<points2.size(); m++) // corresponds to j
                {
                    for(int l=0; l<points2.size(); l++) // corresponds to i
                    {
                        // Find the angle between candidate nodes
                        float theta2 = float(180 * 7.0/22.0) *
                                //                                ( atan2( (points2[l].y - points2[m].y), (points2[l].x - points2[m].x) ) );
                                ( atan( (points2[l].y - points2[m].y) / (points2[l].x - points2[m].x) ) );

                        if(isnan(theta2))
                            theta2 = -90;

                        float r2 = norm(points2[l]-points2[m]);
                        float rErr = r1 > r2 ? r2/r1 : r1/r2;

                        float phi1 = 0;
                        computeBhattacharyaCoefficient(hists1[i],hists2[l],&phi1);
                        float phi2 = 0;
                        computeBhattacharyaCoefficient(hists1[j],hists2[m],&phi2);

                        float scale1 = float(rects1[i].area())/float(rects2[l].area());
                        float scale2 = float(rects1[j].area())/float(rects2[m].area());

                        scale1 = scale1 > 1 ? 1.0/scale1 : scale1;
                        scale2 = scale2 > 1 ? 1.0/scale2 : scale2;

                        // Calcualte edge potential
                        float psi = exp(- 5*abs(theta1-theta2)/100 - 2.5*abs(1-r2/r1)/2 ) * phi1 * phi2 * scale1 * scale2;

                        if(isnan(psi))
                            psi = 0.00001;

                        // Test direction
                        bool testRight = false;
                        if(points2[m].x > points2[l].x)
                            testRight = true;

                        // Reduce the potential if any of the candidates belongs to dummy set
                        if(dummyFlag[l] == 1)
                            psi *= 0.25;
                        if(dummyFlag[m] == 1)
                            psi *= 0.25;

                        //                        cout << factorCounter << "\t" << i << "\t" << j << "\t" << l << "\t" << m << "\t" << theta1 << "\t" <<
                        //                                theta2 << "\t" << psi << "\t" << abs(theta1-theta2) << "\t" << phi1 << "\t" <<
                        //                                phi2 << "\t" << exp(-5*(phi1)/100) << "\t" <<
                        //                                r1 << "\t" << abs(1-r2/r1) << "\t" << r2 << "\t" << exp(-2.5*abs(1-r2/r1)) << "\t" <<
                        //                                scale1 << "\t" << scale2 << endl;


                        bool dummyEntry = false;
                        if(dummyVarIndex[i] == l && dummyVarIndex[j] == m )
                        {
                            if(psi == 0)
                                psi = 0.00001;
                            allZeroStateVal = psi;
                            allZeroStateIndex = factorCounter;
                            dummyEntry = true;
                        }

                        // Mark it zero if it is a dummy entry for both candidates,
                        // Mark it zero if psi is too less or same candidate appears on both edge ends or direction is different
                        if( !dummyEntry && (psi < 0.1 || l==m || right != testRight) )
                        {
                            psi = 0.0;
                        }

                        if(psi != 0 || dummyEntry)
                        {
                            nonZeroStateVal.push_back(psi);
                            nonZeroStateIndex.push_back(factorCounter);
                        }

                        //                        cout << psi << endl;

                        truthTable(l,m) = psi;

                        factorCounter++;
                    }
                }
                edgePotentials.push_back(truthTable);

                if(nonZeroStateVal.size() != 0)
                {
                    factorFile <<  nonZeroStateVal.size() << endl; // Non Zero states
                    for(int i1=0; i1<nonZeroStateVal.size(); i1++)
                        factorFile << nonZeroStateIndex[i1] << " " << nonZeroStateVal[i1] << endl; // factor state index and value
                }
                else
                {
                    factorFile << 1 << endl; // Dummy state
                    factorFile << allZeroStateIndex << " " << allZeroStateVal << endl; // factor state index and value
                }
                factorFile << "\n" << endl;
            }
        }
}


/*!
  @brief Finds MRF-MAP solution for the graph.
  */
bool matchGraph_TS3(vector<Point2f> points1, vector<Point2f> points2, vector<Mat> hists1, vector<Mat> hists2,
                    vector<Rect> rects1, vector<Rect> rects2, Mat_<int> adjMat, string outputPath, vector<int> dummyFlag,
                    vector<int> dummyVarIndex, vector<int>& matchIndex, Mat_<int>& adjMatOut, float& graphMatchProb)
{
    int factorCounter = 0;
    // Find the number of factors in the graph
    for(int i1=0; i1<adjMat.rows; i1++)
        for(int j1=i1+1; j1<adjMat.cols; j1++)
        {
            if(adjMat[i1][j1] != -1)
                factorCounter++;
        }

    // File to store the factors in a particular format
    ofstream factorFile;
    string factorFileName = outputPath;
    factorFileName.append("factor_TS3.fg");
    factorFile.open(factorFileName.c_str());

    factorFile << factorCounter << "\n" << endl;    // Pass the total number of factors

    vector< Mat_<float> > edgePotentials;

    // Function for calculating factors
    getFactors_TS3(factorFile,adjMat,points1,points2,hists1,hists2,rects1,rects2,dummyFlag,dummyVarIndex,edgePotentials);

    // Reading factors from the file (written above)
    dai::FactorGraph fg;
    fg.ReadFromFile(factorFileName.c_str());

    // Set some constants
    size_t maxiter = 10000;
    dai::Real   tol = 1e-9;
    size_t verb = 0;

    // Store the constants in a PropertySet object
    dai::PropertySet opts;
    opts.set("maxiter",maxiter);  // Maximum number of iterations
    opts.set("tol",tol);          // Tolerance for convergence
    opts.set("verbose",verb);     // Verbosity (amount of output generated)

    // Construct another JTree (junction tree) object that is used to calculate
    // the joint configuration of variables that has maximum probability (MAP state)
    dai::JTree mp( fg, opts("updates",string("HUGIN"))("inference",string("MAXPROD")) );
    //    dai::BP mp(fg, opts("updates",string("SEQMAX"))("logdomain",false)("inference",string("MAXPROD"))("damping",string("0.1")));

    // Initialize junction tree algorithm
    mp.init();
    // Run junction tree algorithm
    mp.run();
    // Calculate joint state of all variables that has maximum probability

    // Calculate joint state of all variables that has maximum probability
    // based on the max-product result
    vector<size_t> mpstate = mp.findMaximum();

    // Initialize the output variables
    matchIndex.assign(adjMat.rows,-1);

    for( size_t i = 0; i < mpstate.size(); i++ )
    {
        int nodeLabel = fg.var(i).label();

        matchIndex[nodeLabel] = mpstate[i];
    }

    adjMatOut = Mat_<int>(adjMat.rows,adjMat.cols,-1);

    bool matchOK = false;
    double logScore = 0;
    for( size_t I = 0; I < fg.nrFactors(); I++ )
    {
        // Find labels of the variables involved in the factor
        int nodeLabel1 = dai::Var(fg.factor(I).vars().begin()[0]).label();
        int nodeLabel2 = dai::Var(fg.factor(I).vars().begin()[1]).label();

        // Find indicies of the variables involved in the factor
        int nodeIndex1 = fg.findVar(fg.factor(I).vars().begin()[0]);
        int nodeIndex2 = fg.findVar(fg.factor(I).vars().begin()[1]);

        // Report max-product factor marginals
        //        cout << "Approximate (max-product) MAP factor marginals:" << endl;
        //        cout << mp.belief(fg.factor(I).vars()) << " == " << mp.beliefF(I) << endl;
        //        cout << mp.beliefV(nodeLabel1) << "\t" << mp.beliefV(nodeLabel2) << endl;

        double assignedEdgePotential = edgePotentials[I](mpstate[nodeIndex1],mpstate[nodeIndex2]);
        // Mark the successful matched edges
        if(assignedEdgePotential > 0.1)
        {
            adjMatOut[nodeLabel1][nodeLabel2] = 1;
            matchOK = true;
        }

        logScore += log(assignedEdgePotential);

        Scalar edgePotentialSum = sum(edgePotentials[I]);

    }

    // Report log partition sum of fg, approximated by the belief propagation algorithm
    //    cout << "Approximate (loopy belief propagation) log partition sum: " << mp.logZ() << endl;
    //    cout << "Approximate (max-product) MAP state (log score = " << fg.logScore( mpstate ) << "):" << endl;

    //    graphMatchProb = exp(fg.logScore(mpstate))/exp(mp.logZ());
    //    assert(graphMatchProb <= 1);

    return matchOK;
}


/*!
  @brief Initialize the Kalman Filter for new pairs
  */
void pairKFinit(KalmanFilter& KFtemp, float initVal)
{
    KFtemp.init(2,1,0);
    int dt1 = 0.05;

    KFtemp.transitionMatrix = (Mat_<float>(2, 2) << 1,dt1,0,1); // A

    setIdentity(KFtemp.measurementMatrix); // H
    setIdentity(KFtemp.processNoiseCov, Scalar::all(0.1)); // Q
    setIdentity(KFtemp.measurementNoiseCov, Scalar::all(1)); // R
    setIdentity(KFtemp.errorCovPost, Scalar::all(0.1)); // P

    Mat measure(1,1,CV_32FC1,Scalar(initVal));
    KFtemp.statePre.at<float>(0) = initVal;
    KFtemp.correct(measure);
    KFtemp.predict();

}

/*!
  @brief Updates the Node Relations (i.e. dij, occ_prob and dead agents) for all the trackers.
  */
void updateNodeRelations(map<int,MultiTracker>& Tracker, map< pair<int,int>, KalmanFilter>& relation2)
{
    map<int,MultiTracker>::iterator i1,j1;

    for(i1=Tracker.begin(); i1!=Tracker.end(); i1++)
    {
        int id1 = (*i1).first;
        if(Tracker[id1].agentState == 5)
            continue;
        Rect r1 = Tracker[id1].trackWindow;
        Point2f c1 = 0.5 * (r1.tl() + r1.br());

        j1 = i1;
        j1++;
        for(; j1!=Tracker.end(); j1++)
        {
            int id2 = j1->first;
            if(Tracker[id2].agentState == 5)
                continue;
            Rect r2 = Tracker[id2].trackWindow;
            Point2f c2 = 0.5 * (r2.tl() + r2.br());
            \
            // Don't consider the node relations until trackers start overlapping each other
            //            float olap = findOverlap(r1,r2);
            //            if(olap == 0)
            //                continue;

            float dijNow = norm(c1-c2);
#if(KF_PAIRS)
            Point3f c1_(c1.x,c1.y,r1.area()/10000.f);
            Point3f c2_(c2.x,c2.y,r2.area()/10000.f);
            float dijNow_ = norm(c1_-c2_);
#endif
            if(Tracker[id1].dead || Tracker[id2].dead)
            {
                Tracker[id1].relation.erase(id2);
                Tracker[id2].relation.erase(id1);

                Tracker[id1].relationROIs.erase(id2);
                Tracker[id2].relationROIs.erase(id1);
#if(KF_PAIRS)
                relation2.erase(pair<int,int>(id1,id2));
                relation2.erase(pair<int,int>(id2,id1));
#endif
                continue;
            }
            else if(Tracker[id1].activeTimeSpan > 1 && Tracker[id2].activeTimeSpan > 1)
            {
                float dijOld = Tracker[id1].relation[id2].first;
                float prior = Tracker[id1].relation[id2].second;

                float delta_dij = dijNow - dijOld;
                float deno = dijNow > dijOld ? dijNow : dijOld;

                float likelihood = (1-delta_dij/deno);
                float evidence = likelihood * prior + (2-likelihood) * (1-prior);

                float posterior = (likelihood * prior) / evidence;

                Tracker[id1].relation[id2].second = posterior;
                Tracker[id2].relation[id1].second = posterior;

                Tracker[id1].relation[id2].first = dijNow;
                Tracker[id2].relation[id1].first = dijNow;

#if(KF_PAIRS)
                //                cout << id1 << "\t" << id2 << "\t" <<
                //                        relation2[pair<int,int>(id1,id2)].statePost.at<float>(0)
                //                        << "\t" << Tracker[id2].relation[id1].first << "\t" << Tracker[id2].relation[id1].second << endl;

                if(Tracker[id1].coPedId == -1)
                {
                    Mat measure(1,1,CV_32FC1,Scalar(dijNow_));

                    relation2[pair<int,int>(id1,id2)].correct(measure);
                    relation2[pair<int,int>(id2,id1)].correct(measure);

                    relation2[pair<int,int>(id1,id2)].predict();
                    relation2[pair<int,int>(id2,id1)].predict();
                }
                // Test ioo
                //                else if(Tracker[id1].activeTimeSpan > 2 &&  Tracker[id2].activeTimeSpan > 2)
                //                {
                //                    KalmanFilter tempKF;
                //                    pairKFinit(tempKF,relation2[pair<int,int>(id1,id2)].statePost.at<float>(0));
                //                    for(int i2=0; i2<5; i2++)
                //                    {
                //                        tempKF.predict();
                //                        float dval = tempKF.statePre.at<float>(0) ;
                //                        if(dval<=0)
                //                        {
                //                            cerr << "Test Success for " << id1 << "\t" << id2 << endl;
                //                            break;
                //                        }
                //                        tempKF.correct(Mat());
                //                    }
                //                }

                if(Tracker[id1].activeTimeSpan > 10 && Tracker[id2].activeTimeSpan > 10)
                {
                    // Initial windows
                    Rect r1 = Tracker[id2].relationROIs[id1];
                    Rect r2 = Tracker[id1].relationROIs[id2];

                    Point2f c1 = 0.5 * (r1.tl() + r1.br());
                    Point2f c2 = 0.5 * (r2.tl() + r2.br());

                    Point3f c1_(c1.x,c1.y,r1.area()/10000.f);
                    Point3f c2_(c2.x,c2.y,r2.area()/10000.f);

                    // Current windows
                    Rect r1_ = Tracker[id1].trackWindow;
                    Rect r2_ = Tracker[id2].trackWindow;

                    // Scale change over time
                    float scale1 = float(r1.area())/float(r1_.area());
                    float scale2 = float(r2.area())/float(r2_.area());

                    // Scale change between two agents
                    float scaleChange = float(r1_.area()) / float(r2_.area());
                    scaleChange = scaleChange > 1.0 ? 1/scaleChange : scaleChange;

                    if( ((scale1 > 1 && scale2 > 1) || (scale1 < 1 && scale2 < 1) ) && scaleChange > 0.5
                            && abs(r1_.x-r2_.x) < r1_.width*1.2 )
                    {
                        float dVal = norm(c1-c2);
                        float dValNow = relation2[pair<int,int>(id1,id2)].statePost.at<float>(0);

                        if(abs(dVal-dValNow) < 0.1*r1_.width)
                        {
                            //                            cerr << "------>>> C0-Peds " << id1 << "\t" << id2 << endl;
                            Tracker[id1].coPedId = id2;
                            Tracker[id2].coPedId = id1;
                        }

                        //                        float coef1 = (dVal/r1.area())*r2.area();
                        //                        float coef2 = (dValNow/r1_.area())*r2_.area();

                        //                        float diffCoef = abs(coef1-coef2)/coef1;
                        //                        if(diffCoef < 0.2)
                        //                        {
                        //                            cerr << "------>>> C0-Peds " << id1 << "\t" << id2  << "\t" << diffCoef << endl;
                        //                        }
                    }

                }
#endif
            }
            else
            {
                Tracker[id1].relation[id2].second = 0.5;
                Tracker[id2].relation[id1].second = 0.5;

                Tracker[id1].relation[id2].first = dijNow;
                Tracker[id2].relation[id1].first = dijNow;

                Tracker[id1].relationROIs[id2] = Tracker[id2].trackWindow;
                Tracker[id2].relationROIs[id1] = Tracker[id1].trackWindow;

#if(KF_PAIRS)

                pairKFinit(relation2[pair<int,int>(id1,id2)],dijNow);
                pairKFinit(relation2[pair<int,int>(id2,id1)],dijNow);

#endif
            }

            //            cout << Tracker[id1].relation[id2].second << endl;
        }
    }

}

/*!
  @brief Tests Inter Object Occlusion using new method.
  */
bool testIOO_(MultiTracker tracker, vector<Rect> trackWin, vector<int> trackId, int skipID, float& probOcc, int& occID)
{
    Rect r1 = tracker.trackWindow;

    bool ioo = false;
    float maxVal = 0;
    int maxIdx = -1;
    findMostOverlappingWindow(r1,trackWin,maxIdx,maxVal,skipID);

    if(maxIdx != -1 && maxVal > 0)
    {
        occID = trackId[maxIdx];
        Rect r2 = trackWin[maxIdx];
        float scaleVal = float(r2.area())/float(r1.area());
        scaleVal = scaleVal < 1.0 ? scaleVal : 1;
        probOcc = tracker.relation[occID].second * scaleVal;//occ_prob.at<float>(testID,occID);
        if(probOcc > 0.7)
            ioo = true;
    }
    //    else
    //    {
    //        probOcc = 0;
    //        occID = -1;
    //        map< int, pair<float,float> >::iterator iter1;

    //        for(iter1=tracker.relation.begin(); iter1!=tracker.relation.end(); iter1++)
    //        {
    //            if(iter1->second.second > probOcc)
    //            {
    //                probOcc = iter1->second.second;
    //                occID = iter1->first;
    //            }
    //        }

    //        if(occID != -1 && probOcc > 0.8)
    //            ioo = true;
    //    }

    return ioo;
}


/*!
  @brief Tests if the new available agent is actually an old revocered agent which was occluded.
*/
int testNewAgent(map<int,MultiTracker>& Tracker, Mat img, Rect newAgentPosn, float& finalConf)
{
    finalConf = 0;
    Mat hist1;
    getHist(img,newAgentPosn,hist1);
    Point2f p1 = 0.5 * (newAgentPosn.tl() + newAgentPosn.br());

    vector<float> confVec;
    vector<float> confIdVec;

    for(map<int,MultiTracker>::iterator i=Tracker.begin(); i!=Tracker.end(); i++)
    {
        int id1 = i->first;
        if(Tracker[id1].agentState == 5)
            continue;
        // For agents that are not active/updated and are occluded
        if(Tracker[id1].agentState != 1 && Tracker[id1].dead == false)
        {
            Rect r2 = Tracker[id1].KF_RectPredicted;

            float bhattCoef;
            Mat hist2 = Tracker[id1].hist.clone();
            //            getHist(img,r2,hist2);
            computeBhattacharyaCoefficient(hist1,hist2,&bhattCoef);

            Point2f p2 = 0.5 * (r2.tl() + r2.br());
            float distance = exp(-norm(p1-p2)/300.0);

            float measure1 = (bhattCoef + distance)/2;

            //            cout << measure1 << "\t" << distance << "\t" << bhattCoef << "\t" << newAgentPosn.x << "\t" << id1 << endl;


            //            if(Tracker[id1].agentState == 2)
            //            {
            //                int occID = Tracker[id1].occludingAgentID;

            //                Rect r3 = Tracker[occID].trackWindow;
            ////                Point2f p3 = 0.5 * (r3.tl() + r3.br());

            //                float rightDist = abs(r3.br().x-newAgentPosn.x);
            //                float leftDist = abs(r3.tl().x-newAgentPosn.br().x);
            //                float lessDist = leftDist < rightDist ? leftDist : rightDist;
            //                float dist2 = exp(-leftDist/500.0);

            //                measure1 = (bhattCoef + distance + dist2)/3;

            //                confVec.push_back(measure1);
            ////                cout << measure1 << "\t" << dist2 << "\t" << occID << "\t" << id1 << endl;
            //            }
            //            else
            confVec.push_back(measure1);

            confIdVec.push_back(id1);
        }
    }

    float maxVal = 0;
    int maxIdx = -1;

    for(int i1=0; i1<confVec.size(); i1++)
    {
        if(confVec[i1] > maxVal)
        {
            maxVal = confVec[i1];
            maxIdx = i1;
        }
    }

    if(maxIdx != -1)
    {
        Rect trackerRect = Tracker[confIdVec[maxIdx]].KF_RectPredicted;
        float scaleVal = (float)newAgentPosn.width / (float)trackerRect.width;
        scaleVal = scaleVal > 1 ? 1/scaleVal : scaleVal;
        //        scaleVal = scaleVal > 0.7 ? scaleVal : 0;

        // Calculate drift in assignemnet
        bool drift = false;
        if( abs(trackerRect.x - newAgentPosn.x) > trackerRect.width )
            drift = true;


        //        cout << scaleVal << "\t" << drift << endl;
        if( maxVal > 0.7 && scaleVal > 0.7)
        {
            //            maxVal = (maxVal + scaleVal)/2;
            //            if(maxVal > 0.8)
            //            {
            //            cerr << "Occluding ID " << confIdVec[maxIdx] << " recovered " << confVec[maxIdx] << endl;
            finalConf = 0.5 * (maxVal + scaleVal);
            return confIdVec[maxIdx];
            //            }
        }
        else if(maxVal > 0.5 && Tracker[confIdVec[maxIdx]].agentState == 3 && scaleVal > 0.7 && !drift)
        {
            //            maxVal = (maxVal + scaleVal)/2;
            //            if(maxVal > 0.5)
            //            {
            cerr << "Occluding ID " << confIdVec[maxIdx] << " recovered from Agent State 3 " << confVec[maxIdx] << endl;
            finalConf = 0.5 * (maxVal + scaleVal);
            return confIdVec[maxIdx];
            //            }
        }
        //        else if(maxVal > 0.5 && Tracker[confIdVec[maxIdx]].agentState == 5 && scaleVal > 0.7 )
        //        {
        //            cerr << "Occluding ID " << confIdVec[maxIdx] << " recovered from Agent State 5 " << confVec[maxIdx] << endl;
        //                finalConf = 0.5 * (maxVal + scaleVal);
        //                return confIdVec[maxIdx];
        //        }
        else
            return -1;
    }
    else
    {
        return -1;
    }
}


/*!
  @brief Writes result data on TiXML document (before dead tracker is deleted).
  */
void writeResultFile(MultiTracker tracker, TiXmlElement* root)
{
    if(tracker.activeTimeSpan > 1)
    {
        TiXmlElement* traj_ids = new TiXmlElement("Trajectory");
        root->LinkEndChild(traj_ids);

        int numDetectionFrames = tracker.frameIds.size();

        traj_ids->SetAttribute("obj_id",tracker.id);
        traj_ids->SetAttribute("start_frame",tracker.frameIds[0]);
        traj_ids->SetAttribute("end_frame",tracker.frameIds[numDetectionFrames-1]);

        for(int k1=0; k1<numDetectionFrames; k1++)
        {
            TiXmlElement* id_frames = new TiXmlElement("Frame");
            traj_ids->LinkEndChild(id_frames);

            id_frames->SetAttribute("frame_no",tracker.frameIds[k1]);
            id_frames->SetAttribute("x",tracker.frameROIs[k1].x);
            id_frames->SetAttribute("y",tracker.frameROIs[k1].y);
            id_frames->SetAttribute("width",tracker.frameROIs[k1].width);
            id_frames->SetAttribute("height",tracker.frameROIs[k1].height);
            id_frames->SetAttribute("observation",tracker.frameObservationFlag[k1]);
        }
    }
}


/*!
  @brief Verifies if the tracker should be declared dead or not.
  @param[in,out] tracker    The tracker object.
  @param[in] img            The input image.
  */
bool verifyDeath(MultiTracker& tracker, Mat img)
{
    // Passed the Exit Threshold
    if(tracker.exitTimeSpan >= exitThre /*&& tracker.agentState != 2*/)
        tracker.dead = true;

    // Touches the image boundary
    if(tracker.trackWindow.x <= 0 || tracker.trackWindow.br().x >= img.cols
            || tracker.trackWindow.y <= 0 || tracker.trackWindow.br().y >= img.rows
            || (tracker.status == false && tracker.activeTimeSpan == 1)) // Transients in detection
    {
        tracker.dead = true;
    }

    //    if(tracker.activeTimeSpan < 3 && (tracker.agentState == 3 || tracker.agentState == 4))  // These shall persih, right?
    //        tracker.dead = true;

    if(tracker.dead)
    {
        tracker.agentState = 4;
        cerr << tracker.id << " has expired." << endl;
    }

    return tracker.dead;
}

/*!
  @brief Whenever a tracker dies, these rituals must be performed.
  @param[in,out] Tracker    The map of all IDs and the tracker objects.
  @param[in] id             The input ID that expired.
  @param[out] root          The XML root variable to write output results of the tracker trajectory.
  */
void afterDeathRituals(map<int,MultiTracker>& Tracker, int id, TiXmlElement* root)
{
    map<int,MultiTracker>::iterator i1;

    for(i1=Tracker.begin(); i1!=Tracker.end(); i1++)
    {
        int id1 = i1->first;
        if(Tracker[id1].occludingAgentID == id)
        {
            Tracker[id1].occludingAgentID == -1;
            Tracker[id1].agentState = 4;
        }

#if(KF_PAIRS_PRED)
        if(Tracker[id1].coPedId == id)
        {
            Tracker[id1].coPedId = -1;
        }
#endif

    }

    writeResultFile(Tracker[id],root);
    Tracker.erase(id);
}

/*!
  @brief Decides the final ROI out of the two available based on Bhattacharya Coefficient weighted average.
  */
void decideROI(Mat img, Rect r1, Rect r2, Mat hist, Rect& roiOut)
{
    Mat hist1, hist2;
    float bc1 = 0, bc2 = 0;
    getHist(img,r1,hist1);
    getHist(img,r2,hist2);

    computeBhattacharyaCoefficient(hist,hist1,&bc1);
    computeBhattacharyaCoefficient(hist,hist2,&bc2);

    float normFactor = 1.0/(bc1 + bc2);

    roiOut.width = normFactor * (bc1 * r1.width + bc2 * r2.width);
    roiOut.height = normFactor * (bc1 * r1.height + bc2 * r2.height);

    Point2f center1 = 0.5 * (r1.tl() + r1.br());
    Point2f center2 = 0.5 * (r2.tl() + r2.br());

    Point2f center = normFactor * (bc1 * center1 + bc2 * center2);

    roiOut.x = center.x - roiOut.width/2;
    roiOut.y = center.y - roiOut.height/2;
}


/*!
  @brief Tracking based on Spatial Layout Matching of the available trackers in the frame.
  @param[in] img            Input image to be processed.
  @param[in,out] Tracker    Map of IDs and corresponding tracker objects.
  @param[in] detWin         Detection ROIs.
  @param[in] ouputPath      String containing address of output directory.
  @param[in] frameId        Frame number.
  @param[out] xmlRoot       XML variable for writing results.
  @param[in,out] relation2  Kalman Filter based object-pair distances map
  @param[out] outDoc2       Output file variable to write results.
  */
void track_TS3(Mat img, map<int,MultiTracker>& Tracker, vector<Rect> detWin, string outputPath,
               int frameId, TiXmlElement* xmlRoot, map< pair<int,int>, KalmanFilter>& relation2, ofstream& outdoc2)
{
    vector<Rect> trackWin;  // Contains the track windows being considered for association in this iteration
    vector<int> trackId;    // Corresponding IDs

    // Get the track windows to be associated
    for(map<int,MultiTracker>::iterator i1=Tracker.begin(); i1!=Tracker.end(); i1++)
    {
        int id1 = i1->first;
        if(Tracker[id1].agentState != 5 /*&& Tracker[id1].agentState != 2*/)
        {
            Rect r1;
            r1 = Tracker[id1].trackWindow;
            trackWin.push_back(r1);
            trackId.push_back(id1);
        }
    }

    vector<Point2f> points1;    //  Points in the source graph
    vector<Rect> rects1;        //  Source ROIs
    vector<Mat> hists1;         //  Histograms of the source ROIs
    // Get the centers of track windows
    for(int i1=0; i1<trackWin.size(); i1++)
    {
        Rect r1 = trackWin[i1];
        checkBoundary(img,r1);
        Point2f p1 = 0.5 * (r1.tl() + r1.br());
        points1.push_back(p1);

        Mat hist1;
        getHist(img,r1,hist1);
        hists1.push_back(hist1);
        rects1.push_back(r1);
    }

    // Draw a graph on points (EMST in this case)
    Mat_<int> adjMat;
    vector< vector<int> > nbrList(points1.size());
    Mat imgGraph = img.clone();
    draw_subdiv(imgGraph,points1,adjMat,nbrList,true);

    // Create the target points pool
    vector<Point2f> points2;        // Points for target candidates
    vector<Rect> targetRects;       // Corresponding ROIs
    vector<Mat> hists2;             // Corresponding histograms
    vector<int> dummyFlag;  // a dummy point is added in points2 for each available track window which will be identified using this flag
    vector<int> dummyVarIndex;  // for each available tracker in the source, it contains the index of dummy variable in target

    // Obtain all the rectangles from locally search around each tracker.
/*Modified*/    Mat img1 = img.clone();
/*Modified*/    vector< vector<Rect> > localDetAllRects;
/*Modified*/    vector< vector<int> > dummyFlag_localObs;
/*Modified*/    for (int i1 = 0 ; i1<trackWin.size() ; i1++)
/*Modified*/    {
/*Modified*/        Rect r1 = trackWin[i1];
/*Modified*/        vector <Rect> localDetRects;
/*Modified*/        localDetAllRects.push_back(localDetRects);
/*Modified*/    }
    for(int i1=0; i1<detWin.size(); i1++)
    {
        Point p2 = 0.5 * (detWin[i1].tl() + detWin[i1].br());
        points2.push_back(p2);
        dummyFlag.push_back(0);
        targetRects.push_back(detWin[i1]);

        Mat hist2;
        getHist(img,detWin[i1],hist2);
        hists2.push_back(hist2);
    }

    // Append the dummmy data
    for(map<int,MultiTracker>::iterator i1=Tracker.begin(); i1!=Tracker.end(); i1++)
    {
        int id1 = i1->first;
        if(Tracker[id1].agentState != 5 /*&& Tracker[id1].agentState != 2*/)
        {
            Rect r1 = Tracker[id1].trackWindow;
            Point p1 = 0.5 * (r1.tl() + r1.br());
            points2.push_back(p1);
            dummyFlag.push_back(1);
            targetRects.push_back(r1);
            dummyVarIndex.push_back(dummyFlag.size()-1);

            // Mat hist2 = Tracker[id1].hist;
            Mat hist2;
            getHist(img,r1,hist2);
            hists2.push_back(hist2);

        }
    }
    int count = 0;

    // Append the data obtained from all the rectangles (through local search).
/*Modified*/    for( int i1 =0 ; i1 < localDetAllRects.size() ; i1++)
/*Modified*/    {
/*Modified*/        for (int i2 = 0 ; i2 < localDetAllRects[i1].size() ; i2++)
/*Modified*/        {
/*Modified*/            Rect r1 = localDetAllRects[i1][i2];
//            cout << "Rectangle Details before check boundary : " << r1.tl().x <<","<< r1.tl().y << "--" << r1.size().width << "," << r1.size().height <<endl;
/*Modified*/            checkBoundary(img,r1);
 //           cout << "Rectangle Details After check boundary : " << r1.tl().x <<","<< r1.tl().y << "--" << r1.size().width << "," << r1.size().height <<endl;
/*Modified*/            count++;
/*Modified*/            Point p1 = 0.5 * (r1.tl() + r1.br());
/*Modified*/            points2.push_back(p1);
/*Modified*/            targetRects.push_back(r1);
/*Modified*/            dummyFlag.push_back(2);

/*Modified*/            Mat hist2;
/*Modified*/            getHist(img,r1,hist2);
/*Modified*/            hists2.push_back(hist2);
/*Modified*/        }
/*Modified*/    }

/*Modified*/   // cout << "No. of Detected Rectanges for this frame from localized detected function = " << count <<endl;
    //cout << "OpenCv Detected Rectangles no. = " << detWin.size() <<endl;

    vector<int> matchIndex(points1.size(),-1);      // For each point in source, it stores the corresponding matching index in target
    Mat_<int> adjMatOut;                            // Adjacency matrix for resultant matched graph
    float graphMatchProb = 0;
    bool matchOK = false;
    // Find graph matching solution
    if(points1.size() > 1)
    {
        //        double tempt11 = cv::getTickCount();
        matchOK = matchGraph_TS3(points1,points2,hists1,hists2,rects1,targetRects,adjMat,outputPath,dummyFlag,dummyVarIndex,
                                 matchIndex,adjMatOut,graphMatchProb);
        //        cout << (cv::getTickCount() - tempt11)/cv::getTickFrequency();
    }
    else if(points1.size() == 1)
    {
        Rect r1 = trackWin[0];
        float maxOverlap = -1;
        int maxIndex = -1;
        for(int i1=0; i1<detWin.size(); i1++)
        {
            Rect r2 = detWin[i1];
            float olap = 0;
            calcOverlap(r1.tl(),r2.tl(),r1.size(),r2.size(),&olap);

            if(olap > maxOverlap)
            {
                maxOverlap = olap;
                maxIndex = i1;
            }
        }

        if(maxIndex != -1)
        {
            matchIndex[0] = maxIndex;
        }
    }
    else
    {
        // probably nothing to do
    }

    // Prepare some image for ouput display
    Mat outImgtemp = img.clone();
    if(matchOK)
    {
        // Draw the matching edges
        for(uint i=0; i<adjMatOut.rows; i++)
            for(uint j=i+1; j<adjMatOut.cols; j++)
            {
                if(adjMatOut[i][j]==1)
                {
                    cv::line(outImgtemp,points2[matchIndex[i]],points2[matchIndex[j]],cv::Scalar(0,255,255),2,LINENAME);
                }
            }

        // Draw the match points
        for(int i1=0; i1<matchIndex.size(); i1++)
        {
            if(matchIndex[i1] != -1)
                circle(outImgtemp,points2[matchIndex[i1]],2,Scalar(0,255,0),2,LINENAME);
        }
        //        graphMatchVal = int(matchOK);
    }

    // Draw the target candidates
    Mat targetRectsImg = img.clone();
    for(int i1=0; i1<points2.size(); i1++)
    {
        if(i1<detWin.size())
        {
            circle(targetRectsImg,points2[i1],2,Scalar(0,255,0),2,LINENAME);
            rectangle(targetRectsImg,targetRects[i1],Scalar(0,255,0),2,LINENAME);
        }
        else
        {
            circle(targetRectsImg,points2[i1],2,Scalar(0,255,255),2,LINENAME);
            rectangle(targetRectsImg,targetRects[i1],Scalar(0,255,255),2,LINENAME);
        }
    }

    //    namedWindow("spatialLayout",WINDOW_NORMAL);
    //    imshow("spatialLayout",imgGraph);
    //    namedWindow("targetRectImg",WINDOW_NORMAL);
    //    imshow("targetRectImg",targetRectsImg);
    //    namedWindow("graphMatch",WINDOW_NORMAL);
    //    imshow("graphMatch",outImgtemp);
    //    waitKey(0);

    vector<Rect> foundTrackWins;    // Contains the track windows which were successfully assigned in graph
    vector<int> foundTrackIDs;      // Corresponding IDs
    vector<int> assignedDetFlag(detWin.size(),-1);    // Flag for detections which are assigned to some tracker, others could be new agent
    Mat finalTracksImg = img.clone();
    for(int i1=0; i1<trackId.size(); i1++)
    {
        // Update the agents for whom match was found
        if(matchIndex[i1] != -1 && matchIndex[i1] < detWin.size()) // 1. Check that there is some mapping 2. It shouldn't be a dummy assignment
        {
            Tracker[trackId[i1]].initialAssignment = detWin[matchIndex[i1]];    // Initial Assignment, to be finalized below
            assignedDetFlag[matchIndex[i1]] = 1;
        }

/*Modified*/        else if(matchIndex[i1] != -1 && matchIndex[i1] >= (detWin.size() + trackId.size()))
/*Modified*/        {
/*Modified*/            Tracker[trackId[i1]].initialAssignment = targetRects[matchIndex[i1]];    // Initial Assignment, to be finalized below
/*Modified*/          //  assignedDetFlag[matchIndex[i1]] = 1;
/*Modified*/        //    cout << "One Rectangle confirmed from localized detected function" <<endl;
/*Modified*/           // waitKey(0);
/*Modified*/        }
        // Predict others?
/*Modified*/        else if(matchIndex[i1] >= detWin.size() && matchIndex[i1] < (detWin.size() + trackId.size()))    // The dummy assignments
        {
            // Test for inter object occlusion
            float probOcc = 0;
            int occID = -1;

            bool ioo = testIOO_(Tracker[trackId[i1]],trackWin,trackId,i1,probOcc,occID);

            if(ioo)
            {
                Tracker[trackId[i1]].agentState = 2;
                Tracker[trackId[i1]].occludingAgentID = occID;
#if(DEBUG_STATES)
                cerr << "ID " << trackId[i1] << " occluded by " << occID << " with probability = " << probOcc << endl;
#endif

                // Tests if both objects occlude each other? Assign them the fourth state
                if(Tracker[occID].occludingAgentID == trackId[i1])
                {
                    Tracker[trackId[i1]].agentState = 4;
                    Tracker[occID].agentState = 4;
                }
                Tracker[trackId[i1]].status = false;
            }
            // For BG occlusion or lack of confirmation
            else
            {
                int maxIdx = -1;
                float maxVal = 0;
                findMostOverlappingWindow(trackWin[i1],trackWin,maxIdx,maxVal,i1);
                if(maxIdx == -1)
                {
                    Tracker[trackId[i1]].agentState = 3; // either occluded by BG or are not occluded, but definitely not ioo
#if(DEBUG_STATES)
                    cerr << "Agent " << trackId[i1] << " State is 3" << endl; // Check if it even ever happens
#endif

                }
                else
                {
                    Tracker[trackId[i1]].agentState = 4; // may or may not be ioo
#if(DEBUG_STATES)
                    cerr << "Agent " << trackId[i1] << " State is 4" << endl; // Check if it even ever happens
#endif
                }

                Tracker[trackId[i1]].status = false;
            }
        }


        // Finalize the window to be updated
        if(Tracker[trackId[i1]].initialAssignment.area() != 0)
        {
            Rect r2 = Tracker[trackId[i1]].initialAssignment;
            Rect rOut(0,0,0,0);

            MultiTracker tempTracker = Tracker[trackId[i1]];

///*modified*/                        // Localize using Mean Shift Tracker (Not an important part, experimental, can be commented)
///*modified*/                        float matchVal_MS = 0;
///*modified*/                        tempTracker.trackWindow = r2;
///*modified*/                        tempTracker.localizeAppearanceByMeanShift(img,0.5,&matchVal_MS);
///*modified*/                        rOut = tempTracker.MS_Rect;


            // Finalize using SURF matching
            if(rOut.area() != 0)
            {
                checkBoundary(img,rOut);
                Rect finalROI;
                decideROI(img,r2,rOut,Tracker[trackId[i1]].hist,finalROI);
                rectangle(finalTracksImg,r2,Scalar(0,255,0),2,LINENAME);
                rectangle(finalTracksImg,rOut,Scalar(0,0,255),2,LINENAME);
                r2 = finalROI;
                //                r2.width = detWin[matchIndex[i1]].width;
                //                r2.height = detWin[matchIndex[i1]].height;
            }

            checkBoundary(img,r2);
            rectangle(finalTracksImg,r2,Scalar(255,255,255),2,LINENAME);

            Tracker[trackId[i1]].updateAgent(img,r2,frameId,trackWin);

            foundTrackWins.push_back(r2);
            foundTrackIDs.push_back(trackId[i1]);

        }
    }

    // Initiate new agents for detection responses which were not assigned to any of the trackers
    for(int i1=0; i1<detWin.size(); i1++)
    {
        if(assignedDetFlag[i1] == -1)
        {
            // Remove the overlapping windows to be precise about initiating new trackers
            int maxIdx = -1;
            float maxVal = 0;
            findMostOverlappingWindow(detWin[i1],detWin,maxIdx,maxVal,i1);
            if(maxIdx != -1 && maxVal > 0.4)
            {
                continue;
            }

/*Modified*/            maxIdx = -1;
/*Modified*/            maxVal = 0;
/*Modified*/            findMostOverlappingWindow(detWin[i1],foundTrackWins,maxIdx,maxVal);
/*Modified*/            if(maxIdx != -1 && maxVal > 0.4)
/*Modified*/            {
/*Modified*/            continue;
/*Modified*/            }

            // Tests if the new identified object is actually some old object that has reappeared after occlusion
            float testConf = 0;
            int matchID = testNewAgent(Tracker,img,detWin[i1],testConf);
            if(matchID != -1 && testConf > 0)
            {
                vector<Rect> testRect;
                Rect testROI = detWin[i1];
                scaleRect(testROI,1.2);
                checkBoundary(img,testROI);
                testRect.push_back(testROI);
                bool testOK = false;
                Rect roiOutSurf;
                float roiConf = 0;
                resolveTargetSurf(Tracker[matchID].agentImgUpdated,img,testRect,roiOutSurf,roiConf,testOK);
                roiOutSurf.x += testROI.x;
                roiOutSurf.y += testROI.y;


                if(testOK || testConf > 0.9 )
                {
                    Tracker[matchID].trackWindow = detWin[i1];
                    Tracker[matchID].updateAgent(img,detWin[i1],frameId,trackWin);
                }

            }
            // Initialize a new object Id
            else
            {
                Tracker[globalID].initAgent(img,detWin[i1],frameId,trackWin);
                Tracker[globalID].id = globalID;
                cout << "New Object ID - " << globalID << endl;
                trackWin.push_back(detWin[i1]);
                trackId.push_back(globalID);
                globalID++;
            }
        }
    }


    vector<int> deadIDsCurr;    // Contains all the IDs that expire in this iteration

    // Predict the objects not confirmed by any means
    for(map<int,MultiTracker>::iterator i1=Tracker.begin(); i1!=Tracker.end(); i1++)
    {
        int id1 = i1->first;
        if(Tracker[id1].status == false && Tracker[id1].agentState!=5)
        {
            // Correct the predictor using SURF matching
            if(Tracker[id1].activeTimeSpan > 1)
            {
                vector<Rect> testRect;
                Rect testROI = Tracker[id1].trackWindow;

                scaleRect(testROI,1.2);
                checkBoundary(img,testROI);
                testRect.push_back(testROI);
                bool testOK = false;
                Rect roiOutSurf;
                float roiConf = 0;
                resolveTargetSurf(Tracker[id1].agentImgUpdated,img,testRect,roiOutSurf,roiConf,testOK);
                roiOutSurf.x += testROI.x;
                roiOutSurf.y += testROI.y;
                if(testOK)
                {
                    Rect roiUpdate = testROI;//Tracker[id1].trackWindow;

                    Point2f surfCenter = 0.5 * (roiOutSurf.tl() + roiOutSurf.br());
                    roiUpdate.x = surfCenter.x - roiUpdate.width/2;
                    roiUpdate.y = surfCenter.y - roiUpdate.height/2;

                    checkBoundary(img,roiUpdate);
                    if(roiUpdate.area() > 0)
                    {
                        Tracker[id1].initialPrediction = roiUpdate;
                        Tracker[id1].exitTimeSpan = 0;
                    }
                }


#if(KF_PAIRS_PRED)
                if(Tracker[id1].coPedId != -1)
                {
                    int id2 = Tracker[id1].coPedId;
                    cerr << "Predicting via pairs for " << id1 << "\t" << id2 << endl;
                    float dist = relation2[pair<int,int>(id1,id2)].statePre.at<float>(0);
                    bool left = false;
                    if(Tracker[id1].trackWindow.x < Tracker[id2].trackWindow.x)
                        left = true;
                    Tracker[id1].initialPrediction = Tracker[id1].trackWindow;
                    Tracker[id1].initialPrediction.x = Tracker[id2].trackWindow.x - (left == true ? 1 : -1) * dist;
                }
#endif
            }


            // Update the ioo occluding ID status
            if(Tracker[id1].agentState == 2)
            {
                float probOcc = 0;
                int occID = -1;
                bool ioo = testIOO_(Tracker[id1],foundTrackWins,foundTrackIDs,-1,probOcc,occID);

                if(ioo && occID != Tracker[id1].occludingAgentID)
                {
#if(DEBUG_STATES)
                    cerr << "Update: ID " << id1 << " occluded by " << occID << " with probability = " << probOcc << endl;
#endif
                    Tracker[id1].occludingAgentID = occID;
                }
                else if(!ioo)
                {
#if(DEBUG_STATES)
                    cerr << "Occluding ID " << i1 << " terminated." << endl;
                    Tracker[i1].exitTimeSpan = exitThre;
#endif
                }

            }
            else if(Tracker[id1].agentState == 3)
            {

            }

            predictAgent(Tracker[id1],frameId);
            checkBoundary(img,Tracker[id1].KF_RectPredicted);
            bool dead = verifyDeath(Tracker[id1],img);
            if(dead)
            {
                deadIDsCurr.push_back(id1);
            }
        }
    }

    for(int i1=0; i1<deadIDsCurr.size(); i1++)
        afterDeathRituals(Tracker,deadIDsCurr[i1],xmlRoot);

    //    updateNodeRelations(Tracker,relation2);

    // Writing results corresponding to MOT-Challenge format
    for(int l1=0; l1<trackWin.size(); l1++)
    {
        bool deadID = false;
        for(int m1=0; m1<deadIDsCurr.size(); m1++)
            if(trackId[l1] == deadIDsCurr[m1])
            {
                deadID = true;
                break;
            }

        if(deadID)
            continue;

        if(Tracker[trackId[l1]].activeTimeSpan > 1)
            outdoc2 << frameId+1 << "," << trackId[l1] << "," << trackWin[l1].x << "," << trackWin[l1].y << "," <<
                       trackWin[l1].width << "," << trackWin[l1].height << "," << 1 << "," << -1 << "," <<
                       -1 << "," << -1 << endl;
        }
}
